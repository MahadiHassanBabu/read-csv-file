function initialize() {
	let mapOptions,map,marker,searchBox,latObj,longObj,mapElement,infowindow = "";
		addressEl  = document.getElementsByClassName('map-search'),
		latObjOne  = document.getElementsByClassName('latitude')[0],
		latObjTwo  = document.getElementsByClassName('latitude')[1],
		longObjOne = document.getElementsByClassName('longitude')[0],
		longObjTwo = document.getElementsByClassName('longitude')[1],
		mapElement = document.getElementById('map-canvas');

		mapOptions = {
		 zoom: 4,
		 center: new google.maps.LatLng(23.86985270,90.40520140),
		 disableDefaultUI: false,
		 scrollWheel: true,
		 draggable: true,
		};

		infoWindow = new google.maps.InfoWindow();
		map = new google.maps.Map(mapElement,mapOptions);
		marker = new google.maps.Marker({
		 position: mapOptions.center,
		 map: map,
		 draggable: true
		});
		if (navigator.geolocation){
				navigator.geolocation.getCurrentPosition(position => {
					var latval = position.coords.latitude;
					var lngval = position.coords.longitude;
					latObjTwo.value = latval.toFixed(8);
					longObjTwo.value = lngval.toFixed(8);
					var latLng = new google.maps.LatLng(latval,lngval);
					map.setCenter(latLng);
					marker.setPosition(latLng);
					infoWindow.setContent(`Lat : ${latval} Long : ${lngval}`);
					infoWindow.open(map,marker);
				},function(){

				});
		} else {
				alert("Browser not supported");
		}

  google.maps.event.addListener(map, "click", function(event) {
		 var latval = event.latLng.lat();
	 	 var lngval = event.latLng.lng();
     latObjOne.value = latval.toFixed(8);
     latObjTwo.value = latval.toFixed(8);
     longObjOne.value = lngval.toFixed(8);
     longObjTwo.value = lngval.toFixed(8);
		 marker.setPosition(event.latLng);
		 infoWindow.setContent('Lat : '+latval+' Long : '+lngval);
		 infoWindow.open( map, marker );

	});

	google.maps.event.addListener(marker,"dragend", function ( event ) {
		var lat, long, address;
		lat = marker.getPosition().lat();
		long = marker.getPosition().lng();
		var geocoder = new google.maps.Geocoder();
		geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
	 		if (status == google.maps.GeocoderStatus.OK) {

			}else{
        latObjOne.value = lat.toFixed(8);
        latObjTwo.value = lat.toFixed(8);
        longObjOne.value = long.toFixed(8);
        longObjTwo.value = long.toFixed(8);
				infoWindow.setContent('Lat : '+lat+' Long : '+long);
				infoWindow.open( map, marker );
			}
	  });
	});
}
