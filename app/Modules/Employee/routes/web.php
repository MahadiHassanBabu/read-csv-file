<?php

use App\Modules\Employee\Controllers\EmployeeController;
use Illuminate\Support\Facades\Route;

Route::group(['module' => 'Employee', 'middleware' => ['web'], 'namespace' => 'App\Modules\Employee\Controllers'], function() {

    Route::get('/',[EmployeeController::class,'index'])->name('employee.list');
    Route::post("employees/store", [EmployeeController::class, 'store'])->name("employee.store");

});
