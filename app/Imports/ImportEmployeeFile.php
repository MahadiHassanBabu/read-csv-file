<?php

namespace App\Imports;

use App\Modules\Employee\Models\Employee;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsFailures;

class ImportEmployeeFile implements ToModel, WithHeadingRow, WithValidation, SkipsOnFailure
{
    use Importable, SkipsFailures;

    // use SkipsErrors;

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */

    public function model(array $row)
    {
        return new Employee([
            'name'      => $row['name'],
            'position'  => $row['position'],
            'office'    => $row['office'],
            'age'       => $row['age'],
            'start_date'=> $row['start_date'] ? (is_string($row['start_date']) ? Carbon::parse($row['start_date'])->format('Y-m-d') : \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['start_date'])->format('Y-m-d')) : null
        ]);

    }

    public function rules(): array
    {
        return [
            '*.name'       => 'required',
            '*.position'   => 'required',
            '*.office'     => 'required',
            '*.age'        => 'required',
            '*.start_date' => 'required'
        ];
    }
}
