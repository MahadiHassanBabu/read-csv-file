@extends('layout.app')
@section('header-css')
    {!! Html::style("assets/dist/css/dataTables.bootstrap4.min.css") !!}
    {!! Html::style("assets/dist/css/buttons.dataTables.min.css") !!}
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <h5><i class="fa fa-file-alt"></i> Import Employee CSV OR Excel File</h5>
        </div>
        {!! Form::open(['route'=>'employee.store','method'=>'POST','id'=>'dataForm','enctype'=>'multipart/form-data']) !!}
        <div class="card-body">
            <div class="row">
                <div class="col-md-4 form-group">
                    {!! Form::label('file','CSV OR Excel File : ',['class'=>'required-star']) !!}
                    <br/>
                    <label class="btn btn-default btn-sm">
                        {!! Form::file('attach_file',['id'=>'attachFile']) !!}
                    </label>
                    <p class="text-danger message ml-1 text-bold"></p>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-primary float-right">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>


    @if(isset($dataTable))
        <div class="card">
            <div class="card-body">
                <div class="row mt-4">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            {!! $dataTable->table() !!}
                        </div>
                    </div><!--col-->
                </div><!--row-->
            </div>
        </div>
    @endif
@endsection
@section('footer-script')
    {!! Html::script('assets/dist/js/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/dist/js/dataTables.bootstrap4.min.js') !!}
    {!! Html::script('assets/dist/js/dataTables.buttons.min.js') !!}
    {!! Html::script('assets/dist/js/buttons.server-side.js') !!}
    @if(isset($dataTable))
        {!! $dataTable->scripts() !!}
    @endif

    <script type="text/javascript">
        /**********************
         VALIDATION START HERE
         **********************/
        $(document).on("click", ":submit", function (e) {
            const attachFile = $('#attachFile').val();
            if (!attachFile) {
                e.preventDefault();
                $('.message').html("Please Attach File!");
            }
        });
    </script>
@endsection

