<?php

namespace App\Modules\Employee\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model {

    use HasFactory, SoftDeletes;

    protected $table = 'employees';
    protected $fillable = [
        'id',
        'name',
        'position',
        'office',
        'age',
        'start_date',
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    protected $dates = ['deleted_at'];

    public static function getEmployeeList(){
        return Employee::orderBy('start_date','DESC');
    }

    public static function dbDateFormat($date){
        if($date) {
            $date = str_replace('/','-',$date);
            return Carbon::parse($date)->toDateString();
        }
    }

}
