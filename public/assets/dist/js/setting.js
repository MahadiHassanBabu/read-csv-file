$(document).ready(function () {
  /******************************
  PPG INCOME PROFESSIONAL HIDE EFFECT
   *****************************/
    $(document.body).on("change",".professionType", function() {
        let professionType = $(this).val();
        if (professionType == 'Services')
            $('.serviceDiv').show();
        else
            $('.serviceDiv').hide();
    });
    $(".professionType").trigger("change");

    /******************************
  PPG Service Product Sub Type
   *****************************/
   $('.pre-approve').hide();
    $(document.body).on("change",".app-subtype", function() {
        let appSubType = $(this).val();
        if (appSubType == 'Pre-approved')
            $('.pre-approve').show();
        else
            $('.pre-approve').hide();
    });
    $(".app-subtype").trigger("change");


    $(".app-subtype").on('change',function () {
        let route = "/settings/ppg-configurations/professional-category-by-product-subtype";
        let targetHtml = '.professional-category';
        professionalCategoryByProductSubtype(this,route,targetHtml);
    });

  });
