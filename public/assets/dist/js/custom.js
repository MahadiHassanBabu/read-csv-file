/*********************************
 CONVERT TO DATE FORMAT FUNCTION
 *********************************/
function toDate(dateStr) {
    let dateArr = dateStr.split('/');
    return `${dateArr[2]}-${dateArr[1]}-${dateArr[0]}`;
}

/*********************************
 CONVERT TO DATE FORMAT DB TO DATAPICKER FUNCTION
 *********************************/
function dbToDatepicker(dateStr) {
    let dateArr = dateStr.split('-');
    return `${dateArr[2]}/${dateArr[1]}/${dateArr[0]}`;
}
/*********************************
 CONVERT TO DATE FORMAT FUNCTION
 *********************************/
function dateFormat(date) {
    let dd = String(date.getDate()).padStart(2, '0');
    let mm = String(date.getMonth() + 1).padStart(2, '0');
    let yyyy = date.getFullYear();
    return yyyy + '-' + mm + '-' + dd;
}

/*********************************
 INCREASE DATE DAY FUNCTION
 *********************************/
function dateIncrease(toDayDate) {
    let date = new Date(Date.parse(toDayDate));
    date.setDate(date.getDate() + 1);
    return  date;
}

/*********************************
 DECREASE DATE DAY FUNCTION
 *********************************/
function dateDecrease(toDayDate) {
    let date = new Date(Date.parse(toDayDate));
    date.setDate(date.getDate() - 1);
    return date;
}

// Warn before remove data and redirect
function warnBeforeAction(URL, redirectURL) {
    swal({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-primary",
            confirmButtonText: "Yes, proceed!",
            cancelButtonText: "No, cancel!",
            cancelButtonClass: "btn-danger",
            closeOnConfirm: false,
            closeOnCancel: false,
            showLoaderOnConfirm: true
        },
        function(isConfirm) {
            if (isConfirm) {
                setTimeout(function () {
                    $.ajax({
                        type: "GET",
                        url: URL,
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        success: function(){
                            swal("Done!", "Action done successfully!", "success");
                            window.location.href = redirectURL;
                        }
                    })
                }, 1000);

            } else {
                swal("Cancelled", "Action Cancelled :)", "error");
            }
        });
}

function districtsByDivision(e,route,targetHtml){
    $('.loading_data').hide();
    $(e).after('<span class="loading_data">Loading...</span>');
    let self = $(e);
    let divisionId = $(e).val();
    $.ajax({
        type: "GET",
        url: route,
        data: {
            division_id: divisionId
        },
        success: function (response) {
            let option = '<option value="">Select district</option>';
            if (response.responseCode == 1) {
                $.each(response.data, function (id, value) {
                    option += '<option value="' + id + '">' + value + '</option>';
                });
            }
            $(targetHtml).html(option);
            $(self).next().hide();
        }
    });
}

function upazilasByDistrict(e,route,targetHtml){
    $(e).after('<span class="loading_data">Loading...</span>');
    let self = $(e);
    let districtId = $(e).val();
    $.ajax({
        type: "GET",
        url: route,
        data: {
            district_id: districtId
        },
        success: function (response) {
            let option = '<option value="">Select police station</option>';
            if (response.responseCode == 1) {
                $.each(response.data, function (id, value) {
                    option += '<option value="' + id + '">' + value + '</option>';
                });
            }
            $(targetHtml).html(option);
            $(self).next().hide();
        }
    });
}

function bankTypesByBank(e,route,targetHtml){
    $('.loading_data').hide();
    $(e).after('<span class="loading_data">Loading...</span>');
    let self = $(e);
    let bankId = $(e).val();
    $.ajax({
        type: "GET",
        url: route,
        data: {
            bank_id: bankId
        },
        success: function (response) {
            let option = '<option value="">Select Account Types</option>';
            if (response.responseCode == 1) {
                $.each(response.data, function (id, value) {
                    option += '<option value="' + id + '">' + value + '</option>';
                });
            }
            $(targetHtml).html(option);
            $(self).next().hide();
        }
    });
}

function documentTypesByCategory(e,route,targetHtml){
    $('.loading_data').hide();
    $(e).after('<span class="loading_data">Loading...</span>');
    let self = $(e);
    let documentCategoryId = $(e).val();
    $.ajax({
        type: "GET",
        url: route,
        data: {
            document_category_id: documentCategoryId
        },
        success: function (response) {
            let option = '<option value="">Select document type</option>';
            if (response.responseCode == 1) {
                $.each(response.data, function (id, value) {
                    option += '<option value="' + id + '">' + value + '</option>';
                });
            }
            $(targetHtml).html(option);
            $(self).next().hide();
        }
    });
}

function groupDocumentTypesByCategoryAndProduct(e,route,parentHtml){
    $('.loading_data').hide();
    $(e).after('<span class="loading_data">Loading...</span>');
    let self = $(e);
    let documentCategoryId = $(e).val();
    let productId          = $('#groupProductId').val();
    $.ajax({
        type: "GET",
        url: route,
        data: {
            document_category_id: documentCategoryId,
            product_id: productId,
        },
        success: function (response) {
            let option = '<option value="">Select type</option>';
            if (response.responseCode == 1) {
                $.each(response.data, function (id, value) {
                    option += '<option value="' + id + '">' + value + '</option>';
                });
            }
            $(parentHtml).find('.group_document_type_id').html(option);
            $(self).next().hide();
        }
    });
}

function documentTypesByCategoryAndProduct(e,route,targetHtml){
    $('.loading_data').hide();
    $(e).after('<span class="loading_data">Loading...</span>');
    let self = $(e);
    let documentCategoryId = $(e).val();
    let parentHtml = $(e).parent();
    let productId = $(parentHtml).find('.productId').val();
    $.ajax({
        type: "GET",
        url: route,
        data: {
            document_category_id: documentCategoryId,
            product_id: productId,
        },
        success: function (response) {
            let option = '<option value="">Select document type</option>';
            if (response.responseCode == 1) {
                $.each(response.data, function (id, value) {
                    option += '<option value="' + id + '">' + value + '</option>';
                });
            }
            $(targetHtml).html(option);
            $(self).next().hide();
        }
    });
}

// search branch code by branch id
function codeByBranch(e,route,parentHtml){
    $(e).after('<span class="loading_data">Loading...</span>');
    let self = $(e);
    let branchId = $(e).val();
    $.ajax({
        type: "GET",
        url: route,
        data: {
            branch_id: branchId
        },
        success: function (response) {
            if (response.responseCode == 1) {
                parentHtml.find('.branchCode').val(response.branchCode);
            }
            $(self).next().hide();
        }
    });
}

// search dbr by credit_limit
function dbrByCreditLimit(e,route,params,parentHtml){
    $.ajax({
        type: "GET",
        url: route,
        data: params,
        success: function (response){
          console.log(response.data);
            if (response.responseCode == 1)
                parentHtml.find('.analystDbr').val(response.data);
        }
    });
}

function sisterConcernByCasCifId(e,route,request,parentHtml){
    $(e).after('<span class="loading_data">Loading...</span>');
    let self = $(e);
    let casCifId = request.casCifId;
    parentHtml.find('.cas_cif_id').removeClass('is-invalid');
    $.ajax({
        type: "GET",
        url: route,
        data: {
            cas_cif_id: casCifId
        },
        success: function (response) {
            if (response.responseCode == 1) {
                parentHtml.find('.customer_type').val(response.data.customer_type);
                if(response.data.customer_type == 'organization'){
                    parentHtml.find('.customer_name').val(response.data.business_name);
                    parentHtml.find('.registration').val(response.data.registered_tin);
                }else{
                    parentHtml.find('.customer_name').val(response.data.name);
                }
            }else{
                parentHtml.find('.customer_type').val('');
                parentHtml.find('.customer_name').val('');
                parentHtml.find('.registration').val('');
                parentHtml.find('.customer_name').val('');
                parentHtml.find('.cas_cif_id').addClass('is-invalid');
            }
            $(self).next().hide();
        }
    });
}

function searchNidOrSmartCardByCif(e,route,parentHtml){
    let cifId = $(e).val();
    $.ajax({
        type: "GET",
        url: route,
        data: {
            cas_cif_id: cifId
        },
        success: function (response) {
            if (response.responseCode){
              if(response.data.nid)
              {
                parentHtml.find('.cib-nid-smart-card').html('NID : '+response.data.nid);
              }else {
                parentHtml.find('.cib-nid-smart-card').html('S.Card : '+response.data.smart_card);
              }
            }else{
              parentHtml.find('.cib-nid-smart-card').html('NID OR S.CARD');
            }
        }
    });
}


function employyeNameByEmpId(e,route,parentHtml){
    let empId = $(e).val();
    $.ajax({
        type: "GET",
        url: route,
        data: {
            emp_id: empId
        },
        success: function (response) {
            if (response.responseCode){
              parentHtml.find('.sourceName').val(response.data);
            }
        }
    });
}

function ownerByCasCifId(e,route,request,parentHtml){
    parentHtml.find('.loading_data').hide();
    $(e).after('<span class="loading_data">Loading...</span>');
    let self = $(e);
    let casCifId = request.casCifId;
    parentHtml.find('.select_owner').removeClass('is-invalid');
    $.ajax({
        type: "GET",
        url: route,
        data: {
            cas_cif_id: casCifId
        },
        success: function (response) {
            if (response.responseCode == 1) {
                if(response.data.customer_type == 'person'){
                    parentHtml.find('.owner_name').val(response.data.name);
                }else{
                    parentHtml.find('.owner_name').val(response.data.business_name);
                }
            }else{
                parentHtml.find('.select_owner').addClass('is-invalid');
                parentHtml.find('.owner_name').val('');
            }
            $(self).next().hide();
        }
    });
}

function proposalByCasCifId(e,route,request,parentHtml){
    parentHtml.find('.loading_data').hide();
    $(e).after('<span class="loading_data">Loading...</span>');
    let self = $(e);
    let casCifId = request.casCifId;

    parentHtml.find('.casCifIdResponse').html('');
    $.ajax({
        type: "GET",
        url: route,
        data: {
            cas_cif_id: casCifId
        },
        success: function (response) {
            if (response.responseCode == 1) {
                let innerHtml;
                let customerType = response.data.customerProfile.customer_type;
                if(customerType == 'organization'){
                    innerHtml = `
                        <table class="table table-sm">
                            <tbody>
                            <tr>
                                <th>Trade Licence</th>
                                <td>${response.data.customerProfile.trade_licence}</td>
                            </tr>
                            <tr>
                                <th>Business Name</th>
                                <td>${response.data.customerProfile.business_name}</td>
                            </tr>
                            <tr>
                                <th>Mobile</th>
                                <td>${response.data.customerProfile.mobile}</td>
                            </tr>
                            <tr>
                                <th>Registered TIN</th>
                                <td>${response.data.customerProfile.registered_tin}</td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="2">
                                    <input type="hidden" name="customer_id" value="${response.data.customerProfile.id}">
                                    <input type="hidden" name="cas_cif_id" value="${response.data.customerProfile.cas_cif_id}">
                                    <input type="hidden" name="customer_type" value="${response.data.customerProfile.customer_type}">
                                    <input type="hidden" name="customer_name" value="${response.data.customerProfile.business_name}">
                                    <button name="action_btn" value="generate" class="btn btn-block btn-primary w-100 actionButton"><i class="fa fa-plus-circle"></i> Generate Proposal </button>
                                </td>
                            </tr>
                            </tfoot>
                        </table>`;
                }else{
                    innerHtml = `
                        <table class="table table-sm">
                            <tbody>
                            <tr>
                                <th>NID</th>
                                <td>${response.data.customerProfile.nid}</td>
                            </tr>
                            <tr>
                                <th>Mobile</th>
                                <td>${response.data.customerProfile.mobile}</td>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <td>${response.data.customerProfile.name}</td>
                            </tr>
                            <tr>
                                <th>Date Of Birth</th>
                                <td>${response.data.customerProfile.date_of_birth}</td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="2">
                                    <input type="hidden" name="customer_id" value="${response.data.customerProfile.id}">
                                    <input type="hidden" name="cas_cif_id" value="${response.data.customerProfile.cas_cif_id}">
                                    <input type="hidden" name="customer_type" value="${response.data.customerProfile.customer_type}">
                                    <input type="hidden" name="customer_name" value="${response.data.customerProfile.name}">
                                    <button name="action_btn" value="generate" class="btn btn-block btn-primary w-100 actionButton"><i class="fa fa-plus-circle"></i> Generate Proposal </button>
                                </td>
                            </tr>
                            </tfoot>
                        </table>`;
                }

                innerHtml += `<div class="mt-5" style="overflow-x: auto">
                        <h4>Proposal Details</h4>
                        <table class="table table-striped mt-2">
                            <thead>
                            <tr>
                                <th scope="col">Proposal Id</th>
                                <th scope="col">Product Segment </th>
                                <th scope="col">Product Type</th>
                                <th scope="col">Product Name</th>
                            </tr>
                            </thead>
                            <tbody>`;

                if(response.data.proposals.length){
                    for (proposal of response.data.proposals) {
                        innerHtml += `<tr class="text-center">
                                    <td>${(proposal.proposal_application_id)?proposal.proposal_application_id:'-'}</td>
                                    <td>${(proposal.product_segment)?proposal.product_segment:'-'}</td>
                                    <td>${(proposal.product_type_name)?proposal.product_type_name:'-'}</td>
                                    <td>${(proposal.product_name)?proposal.product_name:'-'}</td>
                                  </tr>`;
                    }
                }else{
                    innerHtml += `<tr class="text-center">
                                    <td colspan="4">No records</td>
                                  </tr>`;
                }

                innerHtml +=`</tbody>
                        </table>
                    </div>
                `;

                parentHtml.find('.casCifIdResponse').html(innerHtml);
            }else{
                let notFound = `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                      <strong>Not found!</strong> CAS CIF ID Not found!
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                </div>`;
                parentHtml.find('.casCifIdResponse').html(notFound);
            }
            $(self).next().hide();
        }
    });
}

function newCasCifIdGenerationFormForProposal(csrfToken,route,parentHtml){
    let innerHtml = `<div class="cas-id-generate">
                        <form method="POST" action="${route}" id="casCifDataForm">
                        <input type="hidden" name="_token" value="${csrfToken}">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Profile Type</label>
                            <div class="col-sm-9">
                                <select name="profile_type" class="required form-control profileType">
                                    <option value="organization">Organization</option>
                                    <option value="person">Person</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Mobile</label>
                            <div class="col-sm-9">
                                <input type="text" minlength='11' maxlength='11' name="mobile" class="form-control required" placeholder="Mobile">
                            </div>
                        </div>
                        <div class="profile-type-div">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">NID</label>
                                <div class="col-sm-9">
                                    <input type="text"  minlength='13' maxlength='17' name="nid" class="form-control required" placeholder="NID">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Name</label>
                                <div class="col-sm-9">
                                    <input type="text" name="name" class="form-control required" placeholder="Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Date of Birth </label>
                                <div class="input-group col-sm-9">
                                    <input type="text" name="date_of_birth" class="date-picker form-control required" placeholder="dd/mm/yyyy" autocomplete="off">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="organization-type-div">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Trade Licence</label>
                                <div class="col-sm-9">
                                    <input type="text" name="trade_licence" class="form-control required" placeholder="Trade licence">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Business Name</label>
                                <div class="col-sm-9">
                                    <input type="text" name="business_name" class="form-control required" placeholder="Business name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Registered TIN</label>
                                <div class="col-sm-9">
                                    <input type="text" name="registered_tin" class="form-control required" placeholder="Registered tin">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <button type="submit" name="action_btn" value="generate" class="btn btn-block btn-primary w-100 actionButton"><i class="fa fa-plus-circle"></i> Generate CAS CIF ID</button>
                            </div>
                        </div>
                        </form>
                    </div>`;
    parentHtml.find('.card-body').html(innerHtml);

}

function productByProductType(e,route,targetHtml){
    $('.loading_data').hide();
    $(e).after('<span class="loading_data">Loading...</span>');
    let self = $(e);
    let productTypeId = $(e).val();
    $.ajax({
        type: "GET",
        url: route,
        data: {
            product_type_id: productTypeId
        },
        success: function (response) {
            let option = '<option value="">Select product</option>';
            if (response.responseCode == 1) {
                $.each(response.data, function (id, value) {
                    option += '<option value="' + id + '">' + value + '</option>';
                });
            }
            $(targetHtml).html(option);
            $(self).next().hide();
        }
    });
}

function productTypeByProductId(e,route,targetHtml){
    let self = $(e);
    let productId = $(e).val();
    $.ajax({
        type: "GET",
        url: route,
        data: {
            product_id: productId
        },
        success: function (response) {
            if (response.responseCode == 1) {

                if(response.data && response.data.product_type_id == 1){
                   targetHtml.find(".creditCardDiv").show();
                   targetHtml.find(".commonDiv").hide();
               }
                else{
                   $(targetHtml).find(".creditCardDiv").hide();
                   $(targetHtml).find(".commonDiv").show();
               }
            }
        }
    });
}

function vehicleCollateralIdSearch(e,route,request,parentHtml){
    parentHtml.find('.loading_data').hide();
    $(e).after('<p class="loading_data">Loading...</p>');
    let self = $(e);
    let collateralId = request.collateralId;
    let source = request.source;
    let formSection = parentHtml.find('.formSection');

    $.ajax({
        type: "GET",
        url: route,
        data: {
            collateral_id: collateralId,
            source: source
        },
        success: function (response) {
            if (response.responseCode == 1) {
                formSection.hide();
                let innerHtml = `<table class="table table-striped mt-2">
                            <thead>
                            <tr>
                                <th scope="col">Collateral ID</th>
                                <th scope="col">Security Code </th>
                                <th scope="col">Vehicle Condition</th>
                                <th scope="col">Manufacturing Year</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="hidden" name="vehicle_id" value="${response.data.id}">${(response.data.collateral_id)?response.data.collateral_id:'-'}</td>
                                    <td>${(response.data.security_code)?response.data.security_code:'-'}</td>
                                    <td>${(response.data.vehicle_condition)?response.data.vehicle_condition:'-'}</td>
                                    <td>${(response.data.manufacturing_year)?response.data.manufacturing_year:'-'}</td>
                                </tr>
                            </tbody>
                        </table>`;

                parentHtml.find('.searchResponseSection').html(innerHtml);
            }else{
                formSection.show();
                let notFound = `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                      <strong>Not found!</strong> Collateral ID Not found!
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                </div>`;
                parentHtml.find('.searchResponseSection').html(notFound);
            }
            $(self).next().hide();
        }
    });
}

function tdrCollateralIdSearch(e,route,request,parentHtml){
    parentHtml.find('.loading_data').hide();
    $(e).after('<p class="loading_data">Loading...</p>');
    let self = $(e);
    let collateralId = request.collateralId;
    let source = request.source;
    let formSection = parentHtml.find('.formSection');

    $.ajax({
        type: "GET",
        url: route,
        data: {
            collateral_id: collateralId,
            source: source
        },
        success: function (response) {
            if (response.responseCode == 1) {
                formSection.hide();
                let innerHtml = `<table class="table table-striped mt-2">
                            <thead>
                            <tr>
                                <th scope="col">Collateral ID</th>
                                <th scope="col">Security Code </th>
                                <th scope="col">TDR Value</th>
                                <th scope="col">TDR A/C Holder</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="hidden" name="tdr_id" value="${response.data.id}">${(response.data.collateral_id)?response.data.collateral_id:'-'}</td>
                                    <td>${(response.data.security_code)?response.data.security_code:'-'}</td>
                                    <td>${(response.data.tdr_value)?response.data.tdr_value:'-'}</td>
                                    <td>${(response.data.tdr_ac_holder)?response.data.tdr_ac_holder:'-'}</td>
                                </tr>
                            </tbody>
                        </table>`;

                parentHtml.find('.searchResponseSection').html(innerHtml);
            }else{
                formSection.show();
                let notFound = `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                      <strong>Not found!</strong> Collateral ID Not found!
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                </div>`;
                parentHtml.find('.searchResponseSection').html(notFound);
            }
            $(self).next().hide();
        }
    });
}

function sndCollateralIdSearch(e,route,request,parentHtml){
    parentHtml.find('.loading_data').hide();
    $(e).after('<p class="loading_data">Loading...</p>');
    let self = $(e);
    let collateralId = request.collateralId;
    let source = request.source;
    let formSection = parentHtml.find('.formSection');

    $.ajax({
        type: "GET",
        url: route,
        data: {
            collateral_id: collateralId,
            source: source
        },
        success: function (response) {
            if (response.responseCode == 1) {
                formSection.hide();
                let innerHtml = `<table class="table table-striped mt-2">
                            <thead>
                            <tr>
                                <th scope="col">Collateral ID</th>
                                <th scope="col">Security Code </th>
                                <th scope="col">SND Balance</th>
                                <th scope="col">SND Rate</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="hidden" name="snd_id" value="${response.data.id}">${(response.data.collateral_id)?response.data.collateral_id:'-'}</td>
                                    <td>${(response.data.security_code)?response.data.security_code:'-'}</td>
                                    <td>${(response.data.snd_balance)?response.data.snd_balance:'-'}</td>
                                    <td>${(response.data.snd_rate)?response.data.snd_rate:'-'}</td>
                                </tr>
                            </tbody>
                        </table>`;

                parentHtml.find('.searchResponseSection').html(innerHtml);
            }else{
                formSection.show();
                let notFound = `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                      <strong>Not found!</strong> Collateral ID Not found!
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                </div>`;
                parentHtml.find('.searchResponseSection').html(notFound);
            }
            $(self).next().hide();
        }
    });
}

function pdcUdcDdiCollateralIdSearch(e,route,request,parentHtml){
    parentHtml.find('.loading_data').hide();
    $(e).after('<p class="loading_data">Loading...</p>');
    let self = $(e);
    let collateralId = request.collateralId;
    let source = request.source;
    let formSection = parentHtml.find('.formSection');

    $.ajax({
        type: "GET",
        url: route,
        data: {
            collateral_id: collateralId,
            source: source
        },
        success: function (response) {
            if (response.responseCode == 1) {
                formSection.hide();
                let innerHtml = `<table class="table table-striped mt-2">
                            <thead>
                            <tr>
                                <th scope="col">Collateral ID</th>
                                <th scope="col">Security Code </th>
                                <th scope="col">PDC Value</th>
                                <th scope="col">Total PDC Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="hidden" name="pdc_udc_ddi_id" value="${response.data.id}">${(response.data.collateral_id)?response.data.collateral_id:'-'}</td>
                                    <td>${(response.data.security_code)?response.data.security_code:'-'}</td>
                                    <td>${(response.data.pdc_number)?response.data.pdc_number:'-'}</td>
                                    <td>${(response.data.total_pdc_amount)?response.data.total_pdc_amount:'-'}</td>
                                </tr>
                            </tbody>
                        </table>`;

                parentHtml.find('.searchResponseSection').html(innerHtml);
            }else{
                formSection.show();
                let notFound = `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                      <strong>Not found!</strong> Collateral ID Not found!
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                </div>`;
                parentHtml.find('.searchResponseSection').html(notFound);
            }
            $(self).next().hide();
        }
    });
}

function hypothecationCollateralIdSearch(e,route,request,parentHtml){
    parentHtml.find('.loading_data').hide();
    $(e).after('<p class="loading_data">Loading...</p>');
    let self = $(e);
    let collateralId = request.collateralId;
    let source = request.source;
    let formSection = parentHtml.find('.formSection');

    $.ajax({
        type: "GET",
        url: route,
        data: {
            collateral_id: collateralId,
            source: source
        },
        success: function (response) {
            if (response.responseCode == 1) {
                formSection.hide();
                let innerHtml = `<table class="table table-striped mt-2">
                            <thead>
                            <tr>
                                <th scope="col">Collateral ID</th>
                                <th scope="col">Security Code </th>
                                <th scope="col">Unit Number</th>
                                <th scope="col">Cost Per Unit</th>
                                <th scope="col">Total Cost</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="hidden" name="hypothecation_id" value="${response.data.id}">${(response.data.collateral_id)?response.data.collateral_id:'-'}</td>
                                    <td>${(response.data.security_code)?response.data.security_code:'-'}</td>
                                    <td>${(response.data.hypothecation_number_of_unit)?response.data.hypothecation_number_of_unit:'-'}</td>
                                    <td>${(response.data.hypothecation_cost_per_unit)?response.data.hypothecation_cost_per_unit:'-'}</td>
                                    <td>${(response.data.hypothecation_total_cost)?response.data.hypothecation_total_cost:'-'}</td>
                                </tr>
                            </tbody>
                        </table>`;

                parentHtml.find('.searchResponseSection').html(innerHtml);
            }else{
                formSection.show();
                let notFound = `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                      <strong>Not found!</strong> Collateral ID Not found!
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                </div>`;
                parentHtml.find('.searchResponseSection').html(notFound);
            }
            $(self).next().hide();
        }
    });
}

function propertyCollateralIdSearch(e,route,request,parentHtml){
    parentHtml.find('.loading_data').hide();
    $(e).after('<p class="loading_data">Loading...</p>');
    let self = $(e);
    let collateralId = request.collateralId;
    let source = request.source;
    let formSection = parentHtml.find('.formSection');

    $.ajax({
        type: "GET",
        url: route,
        data: {
            collateral_id: collateralId,
            source: source
        },
        success: function (response) {
            if (response.responseCode == 1) {
                formSection.hide();
                let innerHtml = `<table class="table table-striped mt-2">
                            <thead>
                            <tr>
                                <th scope="col">Collateral ID</th>
                                <th scope="col">Security Code </th>
                                <th scope="col">Property Nature</th>
                                <th scope="col">Property Type</th>
                                <th scope="col">Owner</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="hidden" name="property_id" value="${response.data.id}">${(response.data.collateral_id)?response.data.collateral_id:'-'}</td>
                                    <td>${(response.data.security_code)?response.data.security_code:'-'}</td>
                                    <td>${(response.data.property_nature)?response.data.property_nature:'-'}</td>
                                    <td>${(response.data.property_type)?response.data.property_type:'-'}</td>
                                    <td>${(response.data.property_owner_name)?response.data.property_owner_name:'-'}</td>
                                </tr>
                            </tbody>
                        </table>`;

                parentHtml.find('.searchResponseSection').html(innerHtml);
            }else{
                formSection.show();
                let notFound = `<div class="alert alert-danger alert-dismissible fade show" role="alert">
                      <strong>Not found!</strong> Collateral ID Not found!
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                </div>`;
                parentHtml.find('.searchResponseSection').html(notFound);
            }
            $(self).next().hide();
        }
    });
}

function financialDataByFinancialType(e){
    let financialTypeId = $(e).val();
    let parentHtml = $(e).parent().parent().parent().parent().parent();
    let proposalId = parentHtml.find('.proposalId').val();

    $.ajax({
        type: "GET",
        url: '/proposal/financial-entry-data-load',
        data: {
            financial_type_id: financialTypeId,
            proposal_id: proposalId
        },
        success: function (response) {
            if (response.responseCode == 1) {
                $(parentHtml).find('select[name="financial_statement_type"]').val(response.data.financial_statement_type);
                $(parentHtml).find('select[name="method"]').val(response.data.method);
                $(parentHtml).find('input[name="auditor_name"]').val(response.data.auditor_name);
                $(parentHtml).find('input[name="financial_assessment_date"]').val(response.data.financial_assessment_date);
                $(parentHtml).find('input[name="financial_start_date"]').val((response.data.financial_start_date)?dbToDatepicker(response.data.financial_start_date):'');
                $(parentHtml).find('input[name="financial_end_date"]').val((response.data.financial_end_date)?dbToDatepicker(response.data.financial_end_date):'');
                $(parentHtml).find('input[name="checked_amount"][value="'+`${response.data.checked_amount}`+'"]').prop("checked",true);
                $(parentHtml).find('input[name="previous_date_one"]').val((response.data.previous_date_one)?dbToDatepicker(response.data.previous_date_one):'');
                $(parentHtml).find('input[name="previous_date_two"]').val((response.data.previous_date_two)?dbToDatepicker(response.data.previous_date_two):'');
                $(parentHtml).find('input[name="previous_date_three"]').val((response.data.previous_date_three)?dbToDatepicker(response.data.previous_date_three):'');
                $(parentHtml).find('input[name="financial_current_date"]').val((response.data.financial_current_date)?dbToDatepicker(response.data.financial_current_date):'');
                $(parentHtml).find('input[name="projected_date"]').val((response.data.projected_date)?dbToDatepicker(response.data.projected_date):'');
            }else{
                $(parentHtml).find('select[name="financial_statement_type"]').val('');
                $(parentHtml).find('select[name="method"]').val('');
                $(parentHtml).find('input[name="auditor_name"]').val('');
                $(parentHtml).find('input[name="financial_assessment_date"]').val();
                $(parentHtml).find('input[name="financial_start_date"]').val('');
                $(parentHtml).find('input[name="financial_end_date"]').val('');
                $(parentHtml).find('input[name="checked_amount"]').prop("checked",false);
                $(parentHtml).find('input[name="previous_date_one"]').val('');
                $(parentHtml).find('input[name="previous_date_two"]').val('');
                $(parentHtml).find('input[name="previous_date_three"]').val('');
                $(parentHtml).find('input[name="financial_current_date"]').val('');
                $(parentHtml).find('input[name="projected_date"]').val('');
            }
        }
    });
}

function financialEntryFormByFinancialType(e,route,parentHtml){
    let financialTypeId = $(e).val();
    let proposalId = parentHtml.find('.proposalId').val();

    let targetHtml = parentHtml.find('.tableAutoloadDiv');
    let financialEntryHtml = '';
    $.ajax({
        type: "GET",
        url: route,
        data: {
            financial_type_id: financialTypeId,
            proposal_id: proposalId
        },
        success: function (response) {
            if (response.responseCode == 1) {

                let financialEntryHeads = response.data;

                if(response.data.length){
                    let headIndex = 0;
                    for(let financialEntryHead of financialEntryHeads){
                        financialEntryHtml += `<div class="col-md-12">
                            <h5 class="mt-3"><input type="hidden" name="financial_head_id[]" value="${financialEntryHead.financial_head_id}">${financialEntryHead.financial_head}</h5>
                            <div style="overflow-x: auto">
                                <div style="overflow-x: auto">
                                    <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-highlight">
                                        <thead>
                                            <th>Sl.</th>
                                            <th>Subhead</th>
                                            <th>Amount</th>
                                            <th>Remarks</th>
                                        </thead>
                                        <tbody>`;
                            let subHeadIndex = 0;
                            for(let financialEntrySubHead of financialEntryHead.financial_sub_heads){
                                financialEntryHtml += `<tr>
                                                    <td>${subHeadIndex + 1}</td>
                                                    <td>${financialEntrySubHead.financial_sub_head_name}<input type="hidden" name="financial_subhead_id[${headIndex}][${subHeadIndex}]" value="${financialEntrySubHead.financial_sub_head_id}"></td>
                                                    <td>
                                                        <div class="d-flex flex-sm-row flex-column">
                                                            <div class="mr-1" style="width:7em"><input value="${(financialEntrySubHead.previous_amount_one)? financialEntrySubHead.previous_amount_one : ''}" type="number" class="form-control" name="previous_amount_one[${headIndex}][${subHeadIndex}]"></div>
                                                            <div class="mr-1" style="width:7em"><input value="${(financialEntrySubHead.previous_amount_two)? financialEntrySubHead.previous_amount_two : ''}" type="number" class="form-control" name="previous_amount_two[${headIndex}][${subHeadIndex}]"></div>
                                                            <div class="mr-1" style="width:7em"> <input value="${(financialEntrySubHead.previous_amount_three)? financialEntrySubHead.previous_amount_three : ''}" type="number" class="form-control" name="previous_amount_three[${headIndex}][${subHeadIndex}]"></div>
                                                            <div class="mr-1" style="width:7em"><input value="${(financialEntrySubHead.current_amount)? financialEntrySubHead.current_amount : ''}" type="number" class="form-control" name="current_amount[${headIndex}][${subHeadIndex}]"></div>
                                                            <div class="mr-1" style="width:7em"><input value="${(financialEntrySubHead.projected_amount)? financialEntrySubHead.projected_amount : ''}" type="number" class="form-control" name="projected_amount[${headIndex}][${subHeadIndex}]"></div>
                                                        </div>
                                                    </td>
                                                    <td><div style="width: 7em"><textarea name="financial_remarks[${headIndex}][${subHeadIndex}]" class="form-control" rows="1">${(financialEntrySubHead.financial_remarks)? financialEntrySubHead.financial_remarks : ''}</textarea></div></td>
                                                </tr>`;
                                subHeadIndex++;
                            }

                        financialEntryHtml += `</tbody>
                                    </table>
                                </div>
                                </div>
                            </div>`;

                        headIndex++;
                    }

                    financialEntryHtml+= `<div class="row mt-3">
                                        <div class="col-md-2 form-group ml-sm-auto">
                                            <button name="actionBtn" id="actionButton" type="submit" value="submit" class="btn btn-primary btn-sm w-100 actionButton"><i class="fa fa-save"></i> Save</button>
                                        </div>
                                    </div>`;
                }

                targetHtml.html(financialEntryHtml);
            }
        }
    });
}

function profileTaggingByCasCifIdOrNID(e,route,request,parentHtml){
    $(e).after('<span class="loading_data">Loading...</span>');
    let self = $(e);
    let casCifId = request.casCifId;
    parentHtml.find(".profile_tagging_cas_cif_id, .profile_tagging_nid").removeClass('is-invalid');
    $.ajax({
        type: "GET",
        url: route,
        data: {
            cas_cif_id: casCifId,
        },
        success: function (response) {
            if (response.responseCode == 1) {
                parentHtml.find('.profile_tagging_cas_cif_id').val(response.data.cas_cif_id);
                parentHtml.find('.profile_tagging_nid').val(response.data.nid);
                if(response.data.customer_type == 'organization'){
                    parentHtml.find('.profile_tagging_name').val(response.data.business_name);
                }else{
                    parentHtml.find('.profile_tagging_name').val(response.data.name);
                }
                parentHtml.find(".profile_tagging_cas_cif_id, .profile_tagging_nid, .profile_tagging_name").attr('readonly', true);
            }else{
                parentHtml.find(".profile_tagging_cas_cif_id, .profile_tagging_nid, .profile_tagging_name").attr('readonly', false);
                parentHtml.find(".profile_tagging_cas_cif_id, .profile_tagging_nid").addClass('is-invalid');
            }
            $(self).next().hide();
        }
    });
}

function referenceTypeByCasCifId(e,route,request,parentHtml){
    $(e).after('<span class="loading_data">Loading...</span>');
    let self = $(e);
    let casCifId = request.casCifId;
    parentHtml.find(".referenceCasCifId").removeClass('is-invalid');
    $.ajax({
        type: "GET",
        url: route,
        data: {
            cas_cif_id: casCifId
        },
        success: function (response) {
            if (response.responseCode == 1) {
                parentHtml.find('.referenceCasCifId').val(response.data.cas_cif_id);
                if(response.data.customer_type == 'organization'){
                    parentHtml.find('.referenceSourceName').val(response.data.business_name);
                }else{
                    parentHtml.find('.referenceSourceName').val(response.data.name);
                }
                parentHtml.find('.referenceDesignation').val(response.data.designation);
                parentHtml.find('.referenceOrganization').val(response.data.organization_name);
                parentHtml.find('.referenceContactNumber').val(response.data.mobile);
                parentHtml.find('.referenceEmail').val(response.data.contact_email);
                parentHtml.find('.referenceOfficePhone').val(response.data.office_tnt_number);
                parentHtml.find('.referenceOrganizationAddress').val(response.data.full_address);
                parentHtml.find(".referenceCasCifId, .referenceSourceName, .referenceDesignation,.referenceOrganization,.referenceOrganization,.referenceContactNumber,.referenceEmail,.referenceOfficePhone,.referenceOrganizationAddress").attr('readonly', true);
            }else{
                parentHtml.find(".referenceCasCifId").addClass('is-invalid');
            }
            $(self).next().hide();
        }
    });
}

function suppleDetailCasCifId(e,route,request,parentHtml){
    let casCifId = request.casCifId;
    parentHtml.find(".suppleCasCifId").removeClass('is-invalid');
    $.ajax({
        type: "GET",
        url: route,
        data: {
            cas_cif_id: casCifId
        },
        success: function (response) {
            if (response.responseCode == 1) {
                parentHtml.find('.suppleCasCifId').val(response.data.cas_cif_id);
                parentHtml.find('.familyTitle').val(response.data.family_title);
                parentHtml.find('.firstName').val(response.data.first_name);
                parentHtml.find('.middleName').val(response.data.middle_name);
                parentHtml.find('.lastName').val(response.data.last_name);
                parentHtml.find('.contactNumber').val(response.data.mobile);
                parentHtml.find('.fatherName').val(response.data.father_name);
                parentHtml.find('.motherName').val(response.data.mother_name);
                parentHtml.find('.nid').val(response.data.nid);
                parentHtml.find('.smartCard').val(response.data.smart_card);
                parentHtml.find('.birthDate').val(dbToDatepicker(response.data.date_of_birth));
                parentHtml.find('.addressOne').val(response.data.address_line_one);
                parentHtml.find('.addressTwo').val(response.data.address_line_two);
                parentHtml.find('.postalCode').val(response.data.postal_code);
                parentHtml.find('.suppleEmailAddress').val(response.data.email);
                // parentHtml.find(".suppleCasCifId, .suppleEmailAddress, .familyTitle, .firstName, .middleName, .lastName, .contactNumber, .fatherName, .motherName, .nid,.smartCard, .birthDate, .addressOne, .addressTwo, .postalCode").removeClass('is-invalid').attr('readonly', true);
            }else{
                parentHtml.find(".suppleCasCifId").addClass('is-invalid');
            }
        }
    });
}

function customerAcknowledgementBorrowerDetailsByProposalId(e,route,request,parentHtml){
    let proposalId = request.proposal_id;
    $.ajax({
        type: "GET",
        url: route,
        data: {
            proposal_id: proposalId
        },
        success: function (response) {
            if (response.responseCode == 1) {
                parentHtml.find('.borrowerName').val(response.data.customer_name);
                parentHtml.find('.tinNo').val(response.data.personal_e_tin);
                parentHtml.find('.designation').val(response.data.designation);
                parentHtml.find('.nid').val(response.data.nid);
                parentHtml.find('.organization').val(response.data.organization_name);
                parentHtml.find('.eTac').val(response.data.mobile);
                parentHtml.find('.birthDate').val(dbToDatepicker(response.data.date_of_birth));
                parentHtml.find('.productName').val(response.data.product_name);
                parentHtml.find('.presentAddress').val(response.data.present_address);
                parentHtml.find('.permanentAddress').val(response.data.permanent_address);
            }
        }
    });
}

function customerAcknowledgementGuarantorDetailsByProposalId(e,route,request,parentHtml){
    let profileTaggingId = request.profile_tagging_id;
    let proposalId = request.proposal_id;
    $.ajax({
        type: "GET",
        url: route,
        data: {
            profile_tagging_id: profileTaggingId,
            proposal_id: proposalId
        },
        success: function (response) {
            if (response.responseCode == 1) {
                parentHtml.find('.borrowerName').val(response.data.name);
                parentHtml.find('.tinNo').val(response.data.personal_e_tin);
                parentHtml.find('.designation').val(response.data.designation);
                parentHtml.find('.nid').val(response.data.nid);
                parentHtml.find('.organization').val(response.data.organization_name);
                parentHtml.find('.eTac').val(response.data.mobile);
                parentHtml.find('.birthDate').val(dbToDatepicker(response.data.date_of_birth));
                parentHtml.find('.presentAddress').val(response.data.full_address);
            } else{
                parentHtml.find(".borrowerName, .tinNo, .designation, .nid, .organization, .birthDate, .productName, .requestAmount, .presentAddress, .eTac").val('');
            }
        }
    });
}

function nomineeByCasCifId(e,route,params,parentHtml){
    $(e).after('<span class="loading_data">Loading...</span>');
    let self = $(e);
    parentHtml.find(".nominee_cas_cif_id,.nominee_name,.nominee_contact_no, .nominee_nationality,.nominee_dob").removeClass('is-invalid');
    parentHtml.find('.nominee_name,.nominee_contact_no, .nominee_nationality,.nominee_dob').val('');
    $.ajax({
        type: "GET",
        url: route,
        data:params,
        success: function (response) {
            if (response.responseCode == 1)
            {
              parentHtml.find('.nominee_cas_cif_id').val(response.data.cas_cif_id);
              if(response.data.customer_type == 'person'){
                parentHtml.find('.nominee_name').val(response.data.name);
                parentHtml.find('.nominee_contact_no').val(response.data.mobile);
                parentHtml.find('.nominee_nationality').val(response.data.nationality);
                parentHtml.find('.nominee_dob').val(dbToDatepicker(response.data.date_of_birth));
              }else{
                parentHtml.find('.nominee_name').val(response.data.business_name);
                parentHtml.find('.nominee_contact_no').val(response.data.mobile);
                parentHtml.find('.nominee_nationality').val(response.data.nationality);
                if(response.data.date_of_birth)
                parentHtml.find('.nominee_dob').val(dbToDatepicker(response.data.date_of_birth));
              }

              let fields = {'.nominee_name':'nominee_name','.nominee_contact_no':'nominee_contact_no','.nominee_nationality':'nominee_nationality','.nominee_dob':'nominee_dob'}

              $.each(fields, function (index, value) {
                  if(parentHtml.find(index).val())
                  {
                    parentHtml.find(index).attr('readonly', true);
                    parentHtml.find(index).css('pointer-events', 'none');
                  }
                  else
                  {
                    parentHtml.find(index).addClass('is-invalid');
                    parentHtml.find(index).attr('readonly', false);
                  }
              });

            }
            else
            {
                parentHtml.find(".nominee_name,.nominee_contact_no, .nominee_nationality, .nominee_dob").attr('readonly', false);
                parentHtml.find('.nominee_nationality,.nominee_dob').css('pointer-events', '');
                parentHtml.find(".nominee_cas_cif_id,.nominee_name,.nominee_contact_no,.nominee_nationality, .nominee_dob").addClass('is-invalid');
            }
            $(self).next().hide();
        }
    });
}

function organizationByOrganizationType(e,route,targetHtml){
    $(e).after('<span class="loading_data">Loading...</span>');
    let self = $(e);
    let organizationTypeId = $(e).val();
    $.ajax({
        type: "GET",
        url: route,
        data: {
            organization_type_id: organizationTypeId
        },
        success: function (response) {
            let option = '<option value="">Select One</option>';
            if (response.responseCode == 1) {
                $.each(response.data, function (id, value) {
                    option += '<option value="' + id + '">' + value + '</option>';
                });
            }
            $(targetHtml).html(option);
            $(self).next().hide();
        }
    });
}

function designationByOrganization(e,route,targetHtml){
    $(e).after('<span class="loading_data">Loading...</span>');
    let self = $(e);
    let organizationId = $(e).val();
    $.ajax({
        type: "GET",
        url: route,
        data: {
            organization_id: organizationId
        },
        success: function (response) {
            let option = '<option value="">Select designation</option>';
            if (response.responseCode == 1) {
                $.each(response.data, function (id, value) {
                    option += '<option value="' + id + '">' + value + '</option>';
                });
            }
            $(targetHtml).html(option);
            $(self).next().hide();
        }
    });
}

function professionalCategoryByProductSubtype(e,route,targetHtml){
    $(e).after('<span class="loading_data">Loading...</span>');
    let self = $(e);
    let targetId = $(e).val();

    $.ajax({
        type: "GET",
        url: route,
        data: {
            application_sub_type: targetId
        },
        success: function (response) {
            let option = '<option value="">Select</option>';
            if (response.responseCode == 1) {
                $.each(response.data, function (id, value) {
                    option += '<option value="' + id + '">' + value + '</option>';
                });
            }
            $(targetHtml).html(option);
            $(self).next().hide();
        }
    });
}

function getBusinessIncomeByCustomer(e,route,request,parentHtml){
    let customerProfileId = request.customer_profile_id;
    $.ajax({
        type: "GET",
        url: route,
        data: {
            customer_profile_id: customerProfileId
        },
        success: function (response) {
            if (response.responseCode == 1)
                parentHtml.find('#monthlyIncomeAmount').val(parseInt(response.data));
            else
                parentHtml.find('#monthlyIncomeAmount').val('');
        }
    });
}
