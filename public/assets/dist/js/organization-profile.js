$(document).ready(function () {
    /****************************
     NEXT PREVIEW BUTTON ACTION
     ***************************/
    $('.btnNext').click(function() {
        $('.nav-pills > .active').next('a').trigger('click');
    });

    $('.btnPrevious').click(function() {
        $('.nav-pills > .active').prev('a').trigger('click');
    });

    /**********************************
     TOP VALIDATION ERROR MESSAGE HIDE
     ********************************/
    $('#v-pills-tab').children('a').click(function(){
        $('.alert').hide();
    })

    /*************************
     SELECT BOX WITH SEARCH
     ***********************/
    $('.districtId,.district,.countryId,.upazilaId').select2({
        width:"100%"
    });


    /****************************
     DATEPICKER ONLY YEAR FORMAT
     ****************************/
    $('#assignmentYear').datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years",
        autoclose: true,
    });



    /******************************
     CONTACT SAME PERMANENT ADDRESS INFO HIDE EFFECT
     *****************************/
    $(document).on("change","#contact-type", function() {
        let type = $(this).val();
        if (type == 'Present' || type == '') {
            $('#is_same_permanent').show();
          }else {
            $('#is_same_permanent').hide();
          }
    });
    $("#contact-type").trigger("change");

    /*********************************************
     BANK TRANSACTION START DATE & END DATE POPUP
     *********************************************/
      $('#bank-start-date').datepicker({
          format: 'dd/mm/yyyy',
          autoclose: true,
          todayHighlight: true,
      }).datepicker("setDate", new Date());

      $('#bank-end-date').datepicker({
           format: 'dd/mm/yyyy',
           autoclose: true,
           todayHighlight: true,
       });

      $('.transaction-summery').hide();
      $(document).on("change","#bank-start-date,#bank-end-date", function() {

         $('#bank-end-date').datepicker('setStartDate',dateIncrease(toDate($('#bank-start-date').val())));
         $('#bank-start-date').datepicker('setEndDate',dateDecrease(toDate($('#bank-end-date').val())));

        if($('#bank-start-date').val() == '' || $('#bank-end-date').val() == '')
            return;

        let startDate = new Date(toDate($('#bank-start-date').val()));
        let endDate = new Date(toDate($('#bank-end-date').val()));
        if(startDate >= endDate){
            $('#bank-end-date').val('');
            $('.transaction-summery').hide();
            return;
        }

        let yearDifference = endDate.getFullYear() - startDate.getFullYear();
        let monthDifference = endDate.getMonth() - startDate.getMonth();

        let difference = (yearDifference*12 + monthDifference);
        let monthYearArray=[];
        let creditSummaryMonth=[];
        let creditSummaryYear=[];

        for(let i = 0 ; i<=difference;i++){
            if(i) startDate.setMonth(startDate.getMonth() + 1);
            monthYearArray[i]= moment(startDate).format("MMMM YYYY");
            creditSummaryMonth[i]= moment(startDate).format("MMMM");
            creditSummaryYear[i]= moment(startDate).format("YYYY");
        }

        let table = `<table class="table table-striped">`;
        for(let i = 0; i < monthYearArray.length; i++){
            table += `<tr>
                            <td>${monthYearArray[i]}</td>
                            <td>
                                <input name="amount[]" type="number" class="form-control" />
                                <input type="hidden" value="${creditSummaryYear[i]}" name="year[]" type="number" class="form-control" />
                                <input type="hidden" value="${creditSummaryMonth[i]}" name="month[]" type="number" class="form-control" />
                            </td>
                        </tr>`;
        }

        table += `</table>`;
        document.getElementById('bankTransactionAmount').innerHTML = table;
        $('.transaction-summery').show();
      });

    $('#bank-start-date,#bank-end-date').trigger('change');



    /************************
     REDIRECT URL START HERE
     ************************/
    let url = document.location.toString();
    if (url.match('#')) {
        $('.nav-pills a[href="#' + url.split('#')[1] + '"]').tab('show');
    }

    /************************************
     SISTER CONCERN AUTOLOAD START HERE
     ************************************/
    $('.sisterConcernCasCifId').on('click',function(){
        let sisterConcern = $(this).parent().parent().parent().parent();
        let casCifId = sisterConcern.find('.cas_cif_id').val();
        let params = {'casCifId':casCifId};

        let route = "/settings/customer-search-by-cas-cif-id";
        sisterConcernByCasCifId(this,route,params,sisterConcern);
    });

    /*****************************************
     OWNER / SHARE HOLDER AUTOLOAD START HERE
     ****************************************/
    $('.select_owner').on('keyup',function(){
        let owner = $(this).parent().parent().parent();
        let casCifId = owner.find('.select_owner').val();
        let params = {'casCifId':casCifId};

        let route = "/settings/customer-search-by-cas-cif-id";
        if(casCifId.length>5){
            ownerByCasCifId(this,route,params,owner);
        }

    });
});

/******************************
 BRANCH CODE ONCHANGE SELECT
 ****************************/
$("#branchId").change(function () {
    let route = "/settings/code-by-branch";
    let targetHtml = '#branchCode';
    codeByBranch(this,route,targetHtml);
});

/*****************************************
 BANK WISE ACCOUNT TYPE ONCHANGE SELECT
 ****************************************/
$(".bankId").change(function () {
    let route = "/settings/account-types-by-bank";
    let targetHtml = '.bankTypeId';
    bankTypesByBank(this,route,targetHtml);
});
