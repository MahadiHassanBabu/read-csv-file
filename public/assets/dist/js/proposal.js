$(document).ready(function () {

    /*******************************************************
     LOAN STRUCTURING PRODUCT CREDIT CARD EFFECT START HERE
     ******************************************************/
    $('.product-id').on('change', function () {
        let parent = $(this).parent().parent().parent().parent().parent();
        let route = "/settings/product-type-by-product";
        productTypeByProductId(this, route, parent);
    });

    $('.product-id').trigger('change');

    /**************************************************
     LOAN STRUCTURING BEFTN WISE DIV EFFECT START HERE
     **************************************************/

    if ($(".beftn-yes").is(":checked"))
        $('#credit-loan-beftn').show();
    else
        $('#credit-loan-beftn').hide();

    $(document).on("change", ".beftn-yes, .beftn-no", function () {
        let parent = $(this).parent().parent().parent().parent().parent();
        let beftn = parent.find('.beftn-yes');

        if (beftn.is(":checked") && beftn.val() == true)
            parent.find('#credit-loan-beftn').show();
        else
            parent.find('#credit-loan-beftn').hide();
    });
    $(".beftn-yes, .beftn-no").trigger("change");

    /***********************************
     PROPERTIES PROPERTY NATURE START
     ***********************************/

    $(document).on("change", "#property-nature", function () {
        let propertyNature = $(this).val();
        if (propertyNature == "structured building") {
            $('#structure-building').show();
            $('#flat').hide();
            $('#vacant-land').hide();
        } else if (propertyNature == "flat") {
            $('#flat').show();
            $('#structure-building').hide();
            $('#vacant-land').hide();
        } else {
            $('#vacant-land').show();
            $('#structure-building').hide();
            $('#flat').hide();
        }

    });

    $("#property-nature").trigger("change");

    /******************
     MANUFACTURER TYPE
     ******************/
    $(document).on("change", "#manufacturer-type", function () {
        let manufacturerType = $(this).val();
        if (manufacturerType == 'History With Other Manufacturer') {
            $('#history-manufacturer').show();
            $('#manufacturer').hide();
        } else {
            $('#manufacturer').show();
            $('#history-manufacturer').hide();
        }
    });

    $("#manufacturer-type").trigger("change");


    /*******************************
     ACCOUNT PROPOSAL GROUP CHECKBOX
     ******************************/

    $("input:checkbox").on('click', function () {
        let $box = $(this);
        if ($box.is(":checked")) {
            let group = "input:checkbox[name='" + $box.attr("name") + "']";
            $(group).prop("checked", false);
            $box.prop("checked", true);
        } else {
            $box.prop("checked", true);
        }
    });

    /*******************************
     ACCOUNT PROPOSAL MODAL EFFECT
     ******************************/
    $('.cas-id-generate').hide(); // first step
    $(".proposalCasId").trigger("change");

    $('.profile-type-div').show(); //second step
    $('.organization-type-div').hide();
    $(".profileType").trigger("change");


    $(document).on('click', '.proposalCasId', function () { // first step functionality
        $('.proposal-cas-div').hide();
        $('.cas-id-generate').show();
    });
    $(document).on('click', '.back', function () {
        $('.proposal-cas-div').show();
        $('.cas-id-generate').hide();
    });

    $(document).on('change', '.profileType', function () { //second step functionality
        let profileType = $(this).val();
        if (profileType == 'person') {
            $('.profile-type-div').show();
            $('.organization-type-div').hide();
        } else {
            $('.profile-type-div').hide();
            $('.organization-type-div').show();
        }

    });

    /***********************************
     LOAN STRUCTURING DIABURSEMENT MODE
     ***********************************/
    let disbursementMode = $('#disbursement-mode').find('option:selected').val();
    if (disbursementMode == "Single" || disbursementMode == "")
        $('#disbursement-multiple').hide();

    $(document).on("change", "#disbursement-mode", function () {
        let mode = $(this).val();
        if (mode == 'Multiple')
            $('#disbursement-multiple').show();
        else
            $('#disbursement-multiple').hide();
    });
    let i = 1;
    $('#disbursement-multiple-add').click(function () {
        i++;
        $('#disbursement-multiple').append('<div id="row' + i + '" class="row mt-2"><div class="col-9"><input type="text" required name="diabursement_multiple[]" placeholder="Disbursment multiple" class="form-control"/></div><div class="col-3"><button type="button" id="' + i + '" class="btn btn-danger btn_remove form-control" name="button">X</button></div></div>');
    });
    $(document).on('click', '.btn_remove', function () {
        let button_id = $(this).attr("id");
        $('#row' + button_id + '').remove();
    });


    /****************************
     Vehicle DATEPICKER ONLY YEAR FORMAT
     ****************************/
    $('#vehicle-model-year,#vehicle-manufacturing-year').datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years",
        autoclose: true,
    });

    /****************************
     NEXT PREVIEW BUTTON ACTION
     ***************************/
    $('.btnNext').click(function () {
        let parent_ref = $('#pills-tab:first li a.active').attr('href');
        $(parent_ref + ' .nav-pills > .active').next('a').trigger('click');
    });

    $('.btnPrevious').click(function () {
        let parent_ref = $('#pills-tab:first li a.active').attr('href');
        $(parent_ref + ' .nav-pills > .active').prev('a').trigger('click');
    });


    /*************************
     SELECT BOX WITH SEARCH
     ***********************/
    $('.countryId,.districtId').select2({
        width: "100%"
    });

    let alert;
    $('#v-pills-tab a').click(function () {
        let url = document.location.toString();
        if ('#' + url.split('#')[1] == $(this).attr('href'))
            $(alert).show()
        else
            alert = $('.alert').hide();
    })

    /************************
     REDIRECT URL START HERE
     ************************/
    let url = document.location.toString();
    if (url.match('#')) {
        $('.nav-pills a[href="#' + url.split('#')[2] + '"]').tab('show');
        $('.nav-pills a[href="#' + url.split('#')[1] + '"]').tab('show');
    }

    /************************************
     CHARGE CREATION  LABEL SCRIPT HERE
     ************************************/
    $('.chargeCreationType,.chargeCreationOn').on('change', function () {
        let parent = $(this).parent().parent().parent().parent();
        let chargeCreationType = parent.find('.chargeCreationType').val();
        let chargeCreationOn = parent.find('.chargeCreationOn').val();

        if (chargeCreationType || chargeCreationOn) {
            let chargeCreationLabel = `${chargeCreationType} on ${chargeCreationOn} Assets of the Company`;
            parent.find('.chargeCreationLabel').html(chargeCreationLabel);
            parent.find('.chargeCreation').val(chargeCreationLabel);
        }
    })
    $(".chargeCreationType,.chargeCreationOn").trigger("change");

    /**************************************
     DOCUMENT GROUP DETAILS
     *************************************/
    $('.document-add-more-entry-row').on('click', function () {
        let parentHtml = $(this).parent().parent().parent().parent().parent();
        let additionalEntryRow = parentHtml.find('.document-additional-entry-row').eq(0).clone();
        let additionalEntryRowIndex = parentHtml.find('.document-additional-entry-row').length;
        additionalEntryRow.find('.document-add-more-entry-row')
            .removeClass('btn-primary')
            .addClass('remove-document-additional-entry-row')
            .addClass('btn-danger')
            .html('<i class="fa fa-minus"></i>');

        additionalEntryRow.find('input,select').each(function (i, input) {
            input.name = input.name.replace('[0]', '[' + additionalEntryRowIndex + ']');
            if (this.name == 'type_of_document_id[' + additionalEntryRowIndex + ']') {
                let option = '<option value="">Select</option>';
                $(this).html(option);
            }
            $(input).val('');
        });
        $('.document-additional-entry-table').append(additionalEntryRow);
    });
    $(document).on('click', '.remove-document-additional-entry-row', function () {
        $(this).parent().parent().remove();
    });

    $(document).on("change", ".group_document_type_id", function () {
        let parentHtml = $(this).parent().parent().parent().parent();
        let parentSibling = $(this).parent().parent();
        let docCatVal = parentSibling.find('.group_document_category_id').val();
        let docTypeVal = $(this).val();
        let totalCount = 0;
        parentHtml.find('.document-additional-entry-row').each(function (i) {
            let compareCatVal = $(this).find('.group_document_category_id').val();
            let compareDocVal = $(this).find('.group_document_type_id').val();
            if (compareCatVal == docCatVal && compareDocVal == docTypeVal)
                totalCount++;
        });
        if (totalCount > 1) {
            $(this).val('');
        }
    });

    /****************
     SOURCE DETAILS
     ****************/
    $('.source-add-more-entry-row').on('click', function () {
        let parentHtml = $(this).parent().parent().parent().parent().parent();
        let additionalEntryRow = parentHtml.find('.source-additional-entry-row').eq(0).clone();
        let additionalEntryRowIndex = parentHtml.find('.source-additional-entry-row').length;
        additionalEntryRow.find('.source-add-more-entry-row')
            .removeClass('btn-primary')
            .addClass('remove-source-additional-entry-row')
            .addClass('btn-danger')
            .html('<i class="fa fa-minus"></i>');

        additionalEntryRow.find('input,select,textarea').each(function (i, input) {
            input.name = input.name.replace('[0]', '[' + additionalEntryRowIndex + ']');
            $(input).val('');
        });
        $('.source-additional-entry-table').append(additionalEntryRow);
    });
    $(document).on('click', '.remove-source-additional-entry-row', function () {
        $(this).parent().parent().remove();
    });

    /************************************
     DBR CALCULATE ONCHANGE CREDIT LIMIT
     ***********************************/

    $(document).on("keyup", ".analystCardLimit", function () {
        let parentHtml = $(this).parent().parent().parent().parent().parent().parent().parent().parent();
        let totalCreditLimit = 0;
        let income = parseFloat(parentHtml.find('.analystIncome').val());
        parentHtml.find('.analystCardLimit').each(function (i, input) {
            if (!isNaN(parseFloat(input.value))) {
                totalCreditLimit += parseFloat(input.value);
            }
        });

        if (!isNaN(income)) {
            let proposalId = parentHtml.find('#analystProposalId').val();
            let params = {'proposal_id': proposalId, 'credit_limit': totalCreditLimit, 'income': income};
            let route = "/proposal/search-dbr-by-credit-limit";
            dbrByCreditLimit(this, route, params, parentHtml);
        } else {
            parentHtml.find('.analystDbr').val('');
        }
    });

    /******************************
     DBR CALCULATE ONCHANGE INCOME
     ******************************/

    $(document).on("keyup", ".analystIncome", function () {
        let parentHtml = $(this).parent().parent().parent().parent().parent().parent().parent();
        let totalCreditLimit = 0;
        let income = parseFloat(parentHtml.find('.analystIncome').val());
        parentHtml.find('.analystCardLimit').each(function (i, input) {
            if (!isNaN(parseFloat(input.value))) {
                totalCreditLimit += parseFloat(input.value);
            }
        });

        if (!isNaN(income)) {
            let proposalId = parentHtml.find('#analystProposalId').val();
            let params = {'proposal_id': proposalId, 'credit_limit': totalCreditLimit, 'income': income};
            let route = "/proposal/search-dbr-by-credit-limit";
            console.log(params);
            dbrByCreditLimit(this, route, params, parentHtml);
        } else {
            parentHtml.find('.analystDbr').val('');
        }
    });

    /*************************
     SOURCE DETAILS SOURCE ID
     *************************/
    $(document).on("keyup", ".sourceId", function () {
        let parentHtml = $(this).parent().parent();
        let parent = $(this).parent().parent().parent();
        let objValue = $(this).val();
        let totalCount = 0;

        parent.find('.sourceId').each(function (i, input) {
            if (input.value == objValue) {
                totalCount++;
            }
        });

        if (totalCount > 1)
            $(this).val('');
        else {
            let route = "/settings/employee-name-by-employee-id";
            employyeNameByEmpId(this, route, parentHtml);
        }
    });

    /*******************************
     SOURCE DETAILS SOURCE CATEGORY
     *******************************/

    $(document).on("change", ".sourceCategory", function () {
        let parentHtml = $(this).parent().parent().parent();
        let objValue = $(this).val();
        let totalCount = 0;
        console.log(objValue);
        parentHtml.find('.sourceCategory').each(function (i, input) {
            if (input.value == objValue) {
                totalCount++;
            }
        });

        if (totalCount > 1)
            $(this).val('');
    });


    /***********************************************************
     GROUP DOCUMENT CATEGORY WISE DOCUMENT TYPE ONCHANGE SELECT
     **********************************************************/

    $(document).on("change", ".group_document_category_id", function () {
        let parentHtml = $(this).parent().parent();
        let route = "/settings/document-types-by-category-and-product";
        groupDocumentTypesByCategoryAndProduct(this, route, parentHtml);
    });

    /***************************************************************
     GROUP DOCUMENT FILE VALIDATION AFTER SUBMISSION ADN SUBMIT FORM
     ***************************************************************/

    //============Ajax Setup===========//
    $(".custom-form").on('submit', function (event) {
        if ($(this).valid()) {
            let form = $(this); //Get Form ID
            let url = form.attr("action"); //Get Form action
            let method = form.attr("method"); //get form's data send method
            let errorMessage = $('.errorMessage'); //get error message div
            let successMessage = $('.successMessage'); //get success message div
            let actionButton = form.find(".actionButton").html();

            $.ajax({
                url: url,
                data: new FormData(this),
                dataType: 'JSON',
                method: method,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function (msg) {
                    form.find(".actionButton").html('<i class="fa fa-cog fa-spin"></i> Processing...');
                    form.find(".actionButton").prop('disabled', true); // disable button
                },
                success: function (data) {
                    //==========validation error===========//
                    if (data.success == false) {
                        errorMessage.hide().empty();
                        $.each(data.error, function (index, error) {
                            errorMessage.removeClass('d-none').append('<li>' + error + '</li>');
                        });
                        errorMessage.slideDown('slow');
                        form.find(".actionButton").html(actionButton);
                        form.find(".actionButton").prop('disabled', false);
                    }
                    //==========if data is saved=============//
                    if (data.success == true) {
                        successMessage.hide().empty();
                        successMessage.removeClass('d-none').html(data.status);
                        successMessage.slideDown('slow');
                        successMessage.delay(2000).slideUp(800, function () {
                            window.location.href = data.link;
                        });
                        form.trigger("reset");

                    }
                    //=========if data already submitted===========//
                    if (data.error == true) {
                        errorMessage.hide().empty();
                        errorMessage.removeClass('d-none').html(data.status);
                        errorMessage.slideDown('slow');
                        form.find(".actionButton").html(actionButton);
                        form.find(".actionButton").prop('disabled', false);
                    }
                },
                error: function (data) {
                    let errors = data.responseJSON;
                    form.find(".actionButton").prop('disabled', false);
                    console.log(errors);
                    alert('Sorry, an unknown Error has been occurred ! Please try again later.');
                }
            });
            return false;
        } else {
            let parent = $(this);
            parent.find('.file-input').each(function (i, input) {
                if (!$(input).val()) {
                    $(input).parent().addClass('is-invalid');
                } else {
                    $(input).parent().removeClass('is-invalid');
                }
            });
        }
    });

    $('.modal-open').click(function () {
        $('#organization-designation-form')[0].reset();
        $('.errorMessage,.successMessage').addClass('d-none');
    });


    /*****************************************************
     INSTANCE ORGANIZATION AND DESIGNATION ADDED INSTANCE
     ****************************************************/


    /*********************************
     GROUP DOCUMENT FILE CHANGE //
     *******************************/
    //  $("#organization-designation-form").on('submit', function (event) {
    //     if ($(this).valid()) {
    //         let form = $(this); //Get Form ID
    //         let url = form.attr("action"); //Get Form action
    //         let method = form.attr("method"); //get form's data send method
    //         let errorMessage = $('.errorMessage'); //get error message div
    //         let successMessage = $('.successMessage'); //get success message div
    //         let actionButton = form.find(".actionButton").html();
    //
    //         $.ajax({
    //             url: url,
    //             data: new FormData(this),
    //             dataType: 'JSON',
    //             method: method,
    //             contentType: false,
    //             cache: false,
    //             processData: false,
    //             beforeSend: function (msg) {
    //                 form.find(".actionButton").html('<i class="fa fa-cog fa-spin"></i> Processing...');
    //                 form.find(".actionButton").prop('disabled', true); // disable button
    //             },
    //             success: function (data) {
    //                 //==========validation error===========//
    //                 if (data.success == false) {
    //                     errorMessage.hide().empty();
    //                     successMessage.addClass('d-none');
    //                     $.each(data.error, function (index, error) {
    //                         errorMessage.removeClass('d-none').append('<li>' + error + '</li>');
    //                     });
    //                     errorMessage.slideDown('slow');
    //                     form.find(".actionButton").html(actionButton);
    //                     form.find(".actionButton").prop('disabled', false);
    //                 }
    //                 //==========if data is saved=============//
    //                 if (data.success == true) {
    //                     successMessage.hide().empty();
    //                     errorMessage.addClass('d-none');
    //                     successMessage.removeClass('d-none').html('<li>Data store successfully</li>');
    //                     successMessage.slideDown('slow');
    //                     let parentHtml = form.parent().parent().parent().parent().parent();
    //                     let organizationId = '<option selected value="' + data.record.organization_id + '">' + data.name + '</option>';
    //                     let designationId = '<option selected value="' + data.designation_id + '">' + data.designation + '</option>';
    //                     parentHtml.find('.organizationId').append(organizationId);
    //                     parentHtml.find('.organizationDesignationId').append(designationId);
    //                     form.find(".actionButton").html(actionButton);
    //                     form.find(".actionButton").prop('disabled', false);
    //                     form[0].reset();
    //                 }
    //                 //=========if data already submitted===========//
    //                 if (data.error == true) {
    //                     errorMessage.hide().empty();
    //                     errorMessage.removeClass('d-none').html(data.status);
    //                     errorMessage.slideDown('slow');
    //                     form.find(".actionButton").html(actionButton);
    //                     form.find(".actionButton").prop('disabled', false);
    //                 }
    //             },
    //             error: function (data) {
    //                 let errors = data.responseJSON;
    //                 form.find(".actionButton").prop('disabled', false);
    //                 console.log(errors);
    //                 alert('Sorry, an unknown Error has been occurred ! Please try again later.');
    //             }
    //         });
    //         return false;
    //     }
    // });

    $(document).on("change", ".file-input", function () {
        $(this).parent().removeClass('is-invalid');
    });

    /**************************************
     CPV REQUEST VERIFICATION  START HERE
     *************************************/
    $('.add-more-row').on('click', function () {
        let parentHtml = $(this).parent().parent().parent();
        let additionalRow = parentHtml.find('.additional-row').eq(0).clone();
        let additionalRowIndex = parentHtml.find('.additional-row').length;
        additionalRow.find('.add-more-row')
            .removeClass('btn-primary')
            .addClass('remove-additional-row')
            .addClass('btn-danger')
            .html('<i class="fa fa-minus-circle"></i>');

        additionalRow.find('select').each(function (i, input) {
            input.name = input.name.replace('[0]', '[' + additionalRowIndex + ']');
            $(input).find('option:selected').removeAttr('selected');
        });
        $('.cpv-request-table').append(additionalRow);
    });
    $(document).on('click', '.remove-additional-row', function () {
        $(this).parent().parent().remove();
    });
    $('.add-more-row').trigger('change');

    $(document).on("change", ".verificationType", function () {
        let parentHtml = $(this).parent().parent().parent().parent().parent();
        let VerificationType = $(this).val();
        let totalCount = 0;
        parentHtml.find('.verificationType').each(function (i, input) {
            if (input.value == VerificationType) {
                totalCount++;
            }
        });

        if (totalCount > 1) {
            $(this).val('');
        }
    });

    /************************************
     FINANCIAL ENTRY SCRIPT START HERE
     ************************************/
    $('.financialStatementType,.financialTypeId').on('change', function () {
        let financialStatementType = $('.financialStatementType').val();
        let financialTypeId = $('.financialTypeId').val();

        if (financialStatementType == 'Audited') {
            $(".auditor").show();
            $('.method-label').removeClass('col-sm-4').addClass('col-sm-3 ml-sm-auto');
            $('.assessment-date-label').removeClass('ml-sm-auto').addClass('col-sm-4');
        } else {
            $(".auditor").hide();
            $('.method-label').removeClass('col-sm-3 ml-sm-auto').addClass('col-sm-4')
            $('.assessment-date-label').removeClass('col-sm-4').addClass('ml-sm-auto');
        }

        if (financialTypeId == 4) {
            $(".financial-balance-type").show();
            $(".assessment-date").hide();
        } else {
            $(".financial-balance-type").hide();
            $(".assessment-date").show();
            $('.assessment-date-label').addClass('ml-sm-auto');
        }
    })
    $(".financialStatementType,.financialTypeId").trigger("change");

    /*****************************************
     RECOMMENDATION LIEN TYPE YES SCRIPT HERE
     ****************************************/
    $('.lien').on('change', function () {
        $(".yesLienDiv").hide();
        let lienValue = $('.lien').val();
        if (lienValue == 'Yes') $(".yesLienDiv").show();
        else $(".yesLienDiv").hide();
    })
    $(".lien").trigger("change");

    /*****************************************
     PRODUCTS BY PRODUCT TYPE ONCHANGE SELECT
     ****************************************/
    $(".productTypeId").change(function () {
        let route = "/settings/products-by-product-type";
        let targetHtml = '.productId';
        productByProductType(this, route, targetHtml);
    });


    /******************************
     VEHICLE COLLATERAL ID SEARCH
     *****************************/
    $('.vehicleCollateralId').on('keyup', function () {
        let parentHtml = $(this).parent().parent().parent().parent();
        let vehicleCollateralId = parentHtml.find('.vehicleCollateralId').val();

        let params = {'collateralId': vehicleCollateralId, 'source': 'vehicles'};

        let route = "/proposals/collateral-id/search";
        if (vehicleCollateralId.length > 5) {
            vehicleCollateralIdSearch(this, route, params, parentHtml);
        }
    });

    /******************************
     PDC UDC DDI COLLATERAL ID SEARCH
     *****************************/
    $('.pdcUdcDdiCollateralId').on('keyup', function () {
        let parentHtml = $(this).parent().parent().parent().parent();
        let pdcUdcDdiCollateralId = parentHtml.find('.pdcUdcDdiCollateralId').val();
        let params = {'collateralId': pdcUdcDdiCollateralId, 'source': 'pdc_udc_ddis'};
        let route = "/proposals/collateral-id/search";
        if (pdcUdcDdiCollateralId.length > 5) {
            parentHtml.find('.searchResponseSection').show();
            pdcUdcDdiCollateralIdSearch(this, route, params, parentHtml);
        } else {
            parentHtml.find('.formSection').show();
            parentHtml.find('.searchResponseSection').hide();
        }
    });

    /*****************************************
     TDR COLLATERAL ID SEARCH
     ****************************************/
    $('.tdrCollateralId').on('keyup', function () {
        let parentHtml = $(this).parent().parent().parent().parent();
        let tdrCollateralId = parentHtml.find('.tdrCollateralId').val();

        let params = {'collateralId': tdrCollateralId, 'source': 'tdrs'};

        let route = "/proposals/collateral-id/search";
        if (tdrCollateralId.length > 5) {
            tdrCollateralIdSearch(this, route, params, parentHtml);
        }
    });

    /*****************************************
     SND COLLATERAL ID SEARCH
     ****************************************/
    $('.sndCollateralId').on('keyup', function () {
        let parentHtml = $(this).parent().parent().parent().parent();
        let sndCollateralId = parentHtml.find('.sndCollateralId').val();

        let params = {'collateralId': sndCollateralId, 'source': 'snds'};
        let route = "/proposals/collateral-id/search";
        if (sndCollateralId.length > 5) {
            sndCollateralIdSearch(this, route, params, parentHtml);
        }
    });


    /************************************
     HYPOTHECATION COLLATERAL ID SEARCH
     ************************************/
    $('.hypothecationCollateralId').on('keyup', function () {
        let parentHtml = $(this).parent().parent().parent().parent();
        let hypothecationCollateralId = parentHtml.find('.hypothecationCollateralId').val();

        let params = {'collateralId': hypothecationCollateralId, 'source': 'hypothecations'};

        let route = "/proposals/collateral-id/search";
        if (hypothecationCollateralId.length > 5) {
            hypothecationCollateralIdSearch(this, route, params, parentHtml);
        }
    });

    /************************************
     PROPERTY COLLATERAL ID SEARCH
     ************************************/
    $('.propertyCollateralId').on('keyup', function () {
        let parentHtml = $(this).parent().parent().parent().parent();
        let propertyCollateralId = parentHtml.find('.propertyCollateralId').val();

        let params = {'collateralId': propertyCollateralId, 'source': 'properties'};

        let route = "/proposals/collateral-id/search";
        if (propertyCollateralId.length > 5) {
            propertyCollateralIdSearch(this, route, params, parentHtml);
        }
    });

    /************************************
     FINANCIAL TYPE WISE SUBHEAD AUTOLOAD
     ************************************/
    $(".financialTypeId").change(function () {
        let route = "/proposal/financial-entry-form-load";
        let parentHtml = $(this).parent().parent().parent().parent().parent();
        financialDataByFinancialType(this);
        financialEntryFormByFinancialType(this, route, parentHtml);
    });
    $(".financialTypeId").trigger('change');

    $(document.body).ready(function () {

        $("#financialEntryForm").validate({
            errorPlacement: function () {
                return true;
            },
//            submitHandler to ajax function
            submitHandler: formSubmit
        });

        let form = $("#financialEntryForm"); //Get Form ID
        let url = form.attr("action"); //Get Form action
        let type = form.attr("method"); //get form's data send method
        let errorMessage = $('.errorMessage'); //get error message div
        let successMessage = $('.successMessage'); //get success message div

        //============Ajax Setup===========//
        function formSubmit() {
            let actionButton = $("#financialEntryForm").find(".actionButton").html();
            $.ajax({
                type: type,
                url: url,
                data: form.serialize(),
                dataType: 'json',
                beforeSend: function (msg) {
                    $("#financialEntryForm").find(".actionButton").html('<i class="fa fa-cog fa-spin"></i> Processing...');
                    $("#financialEntryForm").find(".actionButton").prop('disabled', true); // disable button
                },
                success: function (data) {
                    //==========validation error===========//
                    if (data.success == false) {
                        errorMessage.hide().empty();
                        $.each(data.error, function (index, error) {
                            errorMessage.removeClass('d-none').append('<li>' + error + '</li>');
                        });
                        errorMessage.slideDown('slow');
                        errorMessage.delay(2000).slideUp(1000, function () {
                            $("#financialEntryForm").find(".actionButton").html(actionButton);
                            $("#financialEntryForm").find(".actionButton").prop('disabled', false);
                        });
                    }
                    //==========if data is saved=============//
                    if (data.success == true) {
                        successMessage.hide().empty();
                        successMessage.removeClass('d-none').html(data.status);
                        successMessage.slideDown('slow');
                        successMessage.delay(2000).slideUp(800, function () {
                            window.location.href = data.link;
                        });
                        form.trigger("reset");

                    }
                    //=========if data already submitted===========//
                    if (data.error == true) {
                        errorMessage.hide().empty();
                        errorMessage.removeClass('d-none').html(data.status);
                        errorMessage.slideDown('slow');
                        errorMessage.delay(1000).slideUp(800, function () {
                            $("#financialEntryForm").find(".actionButton").html(actionButton);
                            $("#financialEntryForm").find(".actionButton").prop('disabled', false);
                        });
                    }
                },
                error: function (data) {
                    let errors = data.responseJSON;
                    $("#financialEntryForm").find(".actionButton").prop('disabled', false);
                    console.log(errors);
                    alert('Sorry, an unknown Error has been occurred ! Please try again later.');
                }
            });
            return false;
        }
    });


    /************************************
     CIB REQUEST TABLE CLICK VIEW MODAL
     ************************************/

    $(document.body).on('click', '.CibModal', function (e) {
        e.preventDefault();
        $('#ModalContent').html('<div style="text-align:center;"><h3 class="text-primary">Loading Form...</h3></div>');
        $('#ModalContent').load(
            $(this).parent().attr('data-href'),
            function (response, status, xhr) {
                if (status === 'error') {
                    alert('error');
                    $('#ModalContent').html('<p>Sorry, but there was an error:' + xhr.status + ' ' + xhr.statusText + '</p>');
                }
                return this;
            }
        );
    });

    /*********************************************
     PROFILE TAGGING - SEARCH BY NID / CAS CIF ID
     *********************************************/
    $('.profile_tagging_cas_cif_id_search').on('click', function () {
        let profileTagging = $(this).parent().parent().parent().parent();
        let casCifId = profileTagging.find('.profile_tagging_cas_cif_id').val();
        let params = {'casCifId': casCifId};
        let route = "/settings/customer-search-by-cas-cif-id";
        profileTaggingByCasCifIdOrNID(this, route, params, profileTagging);

    });

    /*********************************************
     REFERENCE TYPE - SEARCH BY CAS CIF ID
     *********************************************/
    $('.referenceCasCifIdSearch').on('click', function () {
        let referenceType = $(this).parent().parent().parent().parent();
        let casCifId = referenceType.find('.referenceCasCifId').val();
        let params = {'casCifId': casCifId};
        let route = "/settings/reference-customer-search-by-cas-cif-id";
        referenceTypeByCasCifId(this, route, params, referenceType);

    });

    /*********************************************
     REFERENCE TYPE - SEARCH BY CAS CIF ID
     *********************************************/
    $('.supple_details_cas_cif_id_search').on('click', function () {
        let suppleDetail = $(this).parent().parent().parent().parent().parent();
        let casCifId = suppleDetail.find('.suppleCasCifId').val();
        let params = {'casCifId': casCifId};
        let route = "/settings/supple-detail-search-by-cas-cif-id";
        suppleDetailCasCifId(this, route, params, suppleDetail);

    });

    /*************************************************
     CUSTOMER ACKNOWLEDGEMENT - SEARCH BY PROPOSAL ID
     *************************************************/
    $('.physicalVerificationToken').on('change', function () {
        let parentHtml = $(this).parent().parent().parent().parent().parent().parent();
        let physicalVerificationToken = $(this).val();
        let proposalId = parentHtml.find('.proposalId').val();
        if (physicalVerificationToken == 'Borrower') {
            let params = {'proposal_id': proposalId};
            let route = "/settings/borrower-details-search-by-proposalId";
            customerAcknowledgementBorrowerDetailsByProposalId(this, route, params, parentHtml);
        }
        if (physicalVerificationToken == 'Guarantor') {
            $('.corporateGuarantor').show();
            parentHtml.find(".borrowerName, .tinNo, .designation, .nid, .organization, .birthDate, .productName, .requestAmount, .presentAddress, .permanentAddress, .eTac").val('');
        } else
            $('.corporateGuarantor').hide();
    });
    $('.physicalVerificationToken, .corporateGuarantor').trigger('change');

    $('.guarantor').on('change', function () {
        let parentHtml = $(this).parent().parent().parent().parent().parent().parent();
        let proposalId = parentHtml.find('.proposalId').val();
        let profileTaggingGuarantorId = $(this).val();
        let params = {'profile_tagging_id': profileTaggingGuarantorId, 'proposal_id': proposalId};
        let route = "/settings/guarantor-details-by-proposalId";
        customerAcknowledgementGuarantorDetailsByProposalId(this, route, params, parentHtml);
    });
    $('.corporateGuarantor').hide();

    /*********************************************
     NOMINEE - SEARCH BY CAS CIF ID
     *********************************************/
    $(document).on("click", "#nominee_cas_cif_id_search", function () {
        let nominee = $(this).parent().parent().parent().parent();
        let casCifId = nominee.find('.nominee_cas_cif_id').val();
        if (casCifId) {
            let params = {'cas_cif_id': casCifId};
            let route = "/settings/customer-search-by-cas-cif-id";
            nomineeByCasCifId(this, route, params, nominee);
        } else
            return false;
    });
    $("#nominee_cas_cif_id_search").trigger("click");


    /**************************************
     FINANCIAL TABLE AUTOLOAD DATE PICKER
     *************************************/
    $(document.body).on('focus', '.financial-date-picker', function () {
        $(this).datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            todayHighlight: true
        });
    });

    /**************************************
     ACCOUNT PROPOSAL SCRIPTING HERE
     *************************************/
    $('.general-card').hide();
    $('.pre-approve').hide();
    $('.app-subtype').on('change', function () {
        let application_sub_type = $(this).val();
        if (application_sub_type == 'General') {
            $('.general-card').show();
            $('.pre-approve').hide();
        } else if (application_sub_type == 'Special') {
            $('.special-card').show();
            $('.general-card').hide();
            $('.pre-approve').hide();
        } else if (application_sub_type == 'Pre-approved') {
            $('.pre-approve').show();
            $('.general-card').hide();
        } else {
            $('.general-card').hide();
            $('.pre-approve').hide();
        }
    });

    /*****************************************
     CPV REQUEST & SUBMISSION FORM VALIDATION
     *****************************************/
    $('#cpv-request-form').validate({
        errorPlacement: function () {
            return false;
        }
    });

    $('#cpv-submission-form').validate({
        errorPlacement: function () {
            return false;
        }
    });

    $(".app-subtype").on('change',function () {
        let route = "/settings/ppg-configurations/professional-category-by-product-subtype";
        let targetHtml = '.professional-category';
        professionalCategoryByProductSubtype(this, route, targetHtml);
    });

    $(".app-subtype").trigger("change");

    $(".forward-role").change(function () {
        let roleId = $(this).val();
        if (roleId != '') {
            $('.submit-button').text('Recommended');
        } else {
            $('.submit-button').text('Approved');
        }
    });
});

/*********************************************
 PROPOSAL REFERENCE TYPE COLLEAGUE AUTO SELECT
 ********************************************/
$('.reference_type').on('change', function () {
    let referenceType = $(this).val();
    if (referenceType == 'Colleague') $('.reference_relationship').prop('selectedIndex', 6);
});
