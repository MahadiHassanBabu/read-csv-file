<?php

namespace App\DataTables\Employee;

use App\Modules\Employee\Models\Employee;
use Carbon\Carbon;
use Yajra\DataTables\Services\DataTable;


class EmployeeDataTable extends DataTable
{

    /**
     * Display ajax response.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return datatables()
            ->eloquent($this->query())
            ->editColumn('start_date', function ($data) {
               return Carbon::parse($data->start_date)->format('Y/m/d') ?? "-";
            })
            ->addColumn('action', function ($data) {
                $actionBtn = '<a href="/employees/' . $data->id . '/edit/" class="btn btn-sm btn-primary" title="Edit"><i class="fa fa-edit"></i> Edit</a> ';
                $actionBtn .= '<a href="/employees/' . $data->id . '/delete/" class="btn btn-sm btn-danger" title="Delete"><i class="fa fa-trash"></i> Delete</a> ';
                return $actionBtn;
            })
            ->rawColumns(['start_date'])
            ->make(true);

    }

    /**
     * Get query source of dataTable.
     * @return \Illuminate\Database\Eloquent\Builder
     * @internal param \App\Models\AgentBalanceTransactionHistory $model
     */
    public function query()
    {
        $data = Employee::getEmployeeList();
        $data->select([
            'employees.*',
        ]);
        return $this->applyScopes($data);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
//            ->addAction(['width' => '160px'])
//            ->minifiedAjax('', null, request()->only(['from', 'to', 'team', 'user', 'category', 'status']))
            ->parameters([
                'dom'         => 'Blfrtip',
                'responsive'  => true,
                'autoWidth'   => false,
                'paging'      => true,
                "pagingType"  => "full_numbers",
                'searching'   => true,
                'info'        => true,
                'searchDelay' => 350,
                "serverSide"  => true,
                'order'       => [[1, 'asc']],
                'buttons'     => ['excel','csv', 'print', 'reset', 'reload'],
                'pageLength'  => 10,
                'lengthMenu'  => [[10, 20, 25, 50, 100, 500, -1], [10, 20, 25, 50, 100, 500, 'All']],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name'      => ['data' => 'name', 'name' => 'name', 'orderable' => true, 'searchable' => true],
            'position'  => ['data' => 'position', 'name' => 'position', 'orderable' => true, 'searchable' => true],
            'office'    => ['data' => 'office', 'name' => 'office', 'orderable' => true, 'searchable' => true],
            'age'       => ['data' => 'age', 'name' => 'age', 'orderable' => true, 'searchable' => true],
            'start_date'=> ['data' => 'start_date', 'name' => 'start_date', 'orderable' => true, 'searchable' => true]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'employee_list_' . date('Y_m_d_H_i_s');
    }
}
