<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    @yield('meta')

    <title>{{ env('APP_NAME','Application') }} | @yield('title','Read CSV OR Excel File')</title>

    <!-- Font Awesome Icons -->
    {!! Html::style('assets/plugins/fontawesome-free/css/all.min.css') !!}
    {!! Html::style('assets/dist/css/adminlte.min.css') !!}
    {!! Html::style('assets/dist/css/custom.css') !!}
    {!! Html::style('assets/toaster/css/toaster.min.css') !!}

    @yield('header-css')
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                @include('includes.messages')
                @yield('content')
            </div>
            <!--/. container-fluid -->
        </section>
        <!-- /.content -->
    </div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
{!! Html::script('assets/plugins/jquery/jquery.min.js') !!}
{!! Html::script('assets/backend/plugins/jquery/jquery.validate.min.js') !!}
{!! Html::script('assets/toaster/js/toaster.min.js') !!}

@if(session()->has('success'))
    {!! Toastr::success(session('success'), 'Success'); !!}
@endif

@if(session()->has('error'))
    {!! Toastr::error(session('error'), 'Error'); !!}
@endif

{!! Toastr::message() !!}

@yield('footer-script')
</body>

</html>
