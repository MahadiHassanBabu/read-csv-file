<?php

namespace App\Modules\Employee\Controllers;

use App\DataTables\Employee\EmployeeDataTable;
use App\Imports\ImportEmployeeFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\HeadingRowImport;

class EmployeeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EmployeeDataTable $dataTable)
    {
        return $dataTable->render("Employee::index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'attach_file' => 'required'
        ]);

        if($request->hasFile('attach_file')){
            $directory = 'employees/attach-file/'; // directory path
            $file = $request->file('attach_file');
            $mimeType = $file->getClientMimeType();

            if(!in_array($mimeType,['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel', 'text/plain','text/csv', 'text/tsv']))
                return redirect()->back()->with('error', 'Only supported file formats are csv, xls or xlsx format!');

            if(!file_exists($directory))
                mkdir($directory, 0777, true);

            $filename = $file->getClientOriginalName();
            $fileOnlyName = pathinfo($filename, PATHINFO_FILENAME);
            $extension = $file->getClientOriginalExtension(); // getting file extension
            $onlyFileName = $fileOnlyName . '_' . rand(11111, 99999);
            $fileName = $onlyFileName . '.' . $extension; // rename of file name
            $path = public_path($directory);
            $file->move($path, $fileName);
            $getFilePath = $path . $fileName;

            $headings = (new HeadingRowImport)->toArray($getFilePath);
            $headingNames = [];
            foreach ($headings as $heading) {
                foreach ($heading as $head) {
                    $headingNames = $head;
                }
            }

            if (count($headingNames) != 5)
                return redirect()->back()->with('error', 'Attach file must have five column in excel sheet');

            if (count($headingNames) != count(array_unique($headingNames)))
                return redirect()->back()->with('error', 'Duplicate column name exist in the attach file.');

            $array = [];
            if ($headingNames[0] != strtolower('Name'))
                array_push($array, ['First column heading name must be Name or name']);

            if ($headingNames[1] != strtolower('Position'))
                array_push($array, ['Second column heading name must be Position or name']);

            if ($headingNames[2] != strtolower('Office'))
                array_push($array, ['Third column heading name must be Office or name']);

            if ($headingNames[3] != strtolower('Age'))
                array_push($array, ['Fourth column heading name must be Age or name']);

            if ($headingNames[4] != strtolower('Start_Date'))
                array_push($array, ['Fifth column heading name must be Start_Date or name']);

            if (count($array) > 0) {
                return back()->withErrors($array);
            }

            $import = new ImportEmployeeFile();
            $import->import($getFilePath);

            if ($import->failures()->isNotEmpty()){
                foreach ($import->failures() as $failure){
                    foreach ($failure->errors() as $error){
                        $errors[] = 'There was en error on row number: ' . $failure->row() . ' ' . $error;
                    }
                }
                return back()->withErrors($errors);
            }
        }
        return redirect()->back()->with('success', 'Data imported successfully.');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
