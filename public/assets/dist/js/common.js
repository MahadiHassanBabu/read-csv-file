/***************************************************
 ORGANIZATION TYPE WISE ORGANIZATION ONCHANGE SELECT
 **************************************************/
$(".organizationTypeId").on('change', function () {
    let route = "/settings/organizations-by-organization-type";
    let targetHtml = '.organizationId';
    organizationByOrganizationType(this, route, targetHtml);
});

/**************************************************
 ORGANIZATION WISE DESIGNATION ONCHANGE SELECT
 **************************************************/
$(".organizationId").on('change',function () {
    let route = "/settings/designations-by-organization";
    let targetHtml = '.organizationDesignationId';
    designationByOrganization(this, route, targetHtml);
});

/******************************
    DATEPICKER
*******************************/

$('.date-picker').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    todayHighlight: true
});

/*************************
 SELECT BOX WITH SEARCH
 ***********************/
$('.districtId,.countryId,.upazilaId,.district,.analystDesignation,.select2').select2({
    width:"100%"
});

/**************************************
DATEPICKER TOP POSITION CHANGE HERE
***************************************/
  let originalCoordinate = 0;
  $('.date-picker').click(function(e){
   if(originalCoordinate != $('.datepicker').position().top)
   {
     $('.datepicker').css({'top':$('.datepicker').position().top+55});
     originalCoordinate = $('.datepicker').position().top;
   }
  });
/************************
 SWEET ALERT START HERE
 ************************/
$(document.body).on('click','.action-delete',function(ev){
    ev.preventDefault();
    let URL = $(this).attr('href');
    let redirectURL = $(this).attr('redirect-url');
    warnBeforeAction(URL, redirectURL);
});


/*********************************************
 DOCUMENT CATEGORY WISE TYPE ONCHANGE SELECT
 *******************************************/
$(".document_category_id").change(function () {
    let route = "/settings/document-types-by-category";
    let targetHtml = '.document_type_id';
    documentTypesByCategory(this,route,targetHtml);
});


/*********************************************
 DOCUMENT CATEGORY AND PRODUCT WISE TYPE ONCHANGE SELECT
 *******************************************/

$(document).on("change", ".proposal_document_category_id", function () {
    let route = "/settings/document-types-by-category-and-product";
    let targetHtml = '.document_type_id';
    documentTypesByCategoryAndProduct(this,route,targetHtml);
});

$(".proposal_document_category_id").trigger("change");


/*****************************
 EDIT MODAL EFFECT START HERE
 *****************************/
$(document.body).on('click','.AppModal',function(e){
    e.preventDefault();
    $('#ModalContent').html('<div style="text-align:center;"><h3 class="text-primary">Loading Form...</h3></div>');
    $('#ModalContent').load(
        $(this).attr('href'),
        function (response, status, xhr) {
            if (status === 'error') {
                alert('error');
                $('#ModalContent').html('<p>Sorry, but there was an error:' + xhr.status + ' ' + xhr.statusText + '</p>');
            }
            return this;
        }
    );
});

/******************************
 ADDITIONAL ADD INSTANCE MODAL
 *****************************/
$(document).on('click','.InstanceModal',function(e){
    e.preventDefault();
    $('#InstanceModalContent').html('<div style="text-align:center;"><h3 class="text-primary">Loading Form...</h3></div>');
    $('#InstanceModalContent').load(
        $(this).attr('redirect-url'),
        function (response, status, xhr) {
            if (status === 'error') {
                alert('error');
                $('#ModalContent').html('<p>Sorry, but there was an error:' + xhr.status + ' ' + xhr.statusText + '</p>');
            }
            return this;
        }
    );
});



/**********************************
 DIVISION ONCHANGE SELECT DISTRICT
 *********************************/
$(document.body).on('change','.divisionId',function(e){
    let route = "/settings/districts-by-division";
    let targetHtml = '.districtId';
    districtsByDivision(this,route,targetHtml);
});


/***************************************
 DISTRICT WISE UPAZILA ONCHANGE SELECT
 ***************************************/
$(".districtId").change(function () {
    let route = "/settings/upazilas-by-district";
    let targetHtml = '.upazilaId';
    upazilasByDistrict(this,route,targetHtml);
});


/******************************
 Image preview
 *****************************/

 $('.imageChange').on('change',function(){
     let parentHtml = $(this).parent().parent();
     let  viewImageId  = parentHtml.find('.viewImage');
     let  errorImageId = parentHtml.find('.errorImage');
     errorImageId .html('');
     if (this.files && this.files[0])
     {
      let mime_type = this.files[0].type;
      if (!(mime_type == 'image/jpeg' || mime_type == 'image/jpg' || mime_type == 'image/png')) {
          errorImageId .html("Invalid file format Only jpg jpeg png is allowed");
          return false;
      }
      let size = this.files[0].size;
      if(size > 3000000){
       errorImageId .html("Please upload image must less than 1MB!!");
       return false;
      }
      let reader = new FileReader();
      reader.onload = function (e) {
          viewImageId.attr('src', e.target.result);
      };
      reader.readAsDataURL(this.files[0]);
    }
 });
