$(document).ready(function () {

    /******************************
     eKYC & CAS CIF ID HIDE EFFECT
     *****************************/
    $('.cas-div').show();
    $('.ekyc-div').hide();

    $('#ekyc').click(function(){
        $('.cas-div').hide();
        $('.ekyc-div').show();
        $("#ekyc").attr("disabled", true).removeClass('btn-primary').addClass('btn-secondary');
        $("#casCif").attr("disabled", false).removeClass('btn-secondary').addClass('btn-primary');
    });

    $('#casCif').click(function(){
        $('.cas-div').show();
        $('.ekyc-div').hide();
        $("#casCif").attr("disabled", true).removeClass('btn-primary').addClass('btn-secondary');
        $("#ekyc").attr("disabled", false).removeClass('btn-secondary').addClass('btn-primary');
    });

    /****************************
     NEXT PREVIEW BUTTON ACTION
     ***************************/
    $('.btnNext').click(function() {
        $('.nav-pills > .active').next('a').trigger('click');
    });

    $('.btnPrevious').click(function() {
        $('.nav-pills > .active').prev('a').trigger('click');
    });


    /******************************
     PERSONAL INFO EMPLOYEE STATUS HIDE EFFECT
     *****************************/

   $(document).on("change", ".employee-status", function () {
       let employeeStatus = $(this).val();
       let parent = $(this).parent().parent().parent().parent();
       if(employeeStatus == 'Employed')
        parent.find('.employee-status-div').show();
        else
        {
          parent.find('.employee-status-div input,textarea').each(function(i,input){
              console.log(input.name);
              $(input).val('');
          });
          parent.find('.employee-status-div').hide();
        }
   });

   $(".employee-status").trigger("change");
    /******************************
     PERSONAL INFO MARITAL STATUS HIDE EFFECT
     *****************************/

   $(document).on("change", "#person-marital-status", function () {
       let maritalStatus = $(this).val();
       let parent = $(this).parent().parent().parent().parent().parent().parent();
       if(maritalStatus == 'SINGLE')
       {
         let spouseDiv =  parent.find('#spouse-div')
         spouseDiv.find('input,select,textarea').each(function(i,input){
             console.log(input.name);
             $(input).val('');
         });
         spouseDiv.hide();
       }
        else
        parent.find('#spouse-div').show();
   });

   $("#person-marital-status").trigger("change");

    /**********************************
     TOP VALIDATION ERROR MESSAGE HIDE
     ********************************/
     let alert;
    $('#v-pills-tab a').click(function(){
      let url = document.location.toString();
      if ('#'+url.split('#')[1] == $(this).attr('href'))
      $(alert).show()
      else
      alert = $('.alert').hide();
    })

    /*********************************************
     BANK TRANSACTION START DATE & END DATE POPUP
     *********************************************/
     $('#bank-start-date').datepicker({
         format: 'dd/mm/yyyy',
         autoclose: true,
         todayHighlight: true,
     }).datepicker("setDate", new Date());

     $('#bank-end-date').datepicker({
          format: 'dd/mm/yyyy',
          autoclose: true,
          todayHighlight: true,
      });

     $('.transaction-summery').hide();
     $(document).on("change","#bank-start-date,#bank-end-date", function() {

        $('#bank-end-date').datepicker('setStartDate',dateIncrease(toDate($('#bank-start-date').val())));
        $('#bank-start-date').datepicker('setEndDate',dateDecrease(toDate($('#bank-end-date').val())));

       if($('#bank-start-date').val() == '' || $('#bank-end-date').val() == '')
           return;

       let startDate = new Date(toDate($('#bank-start-date').val()));
       let endDate = new Date(toDate($('#bank-end-date').val()));
       if(startDate >= endDate){
           $('#bank-end-date').val('');
           $('.transaction-summery').hide();
           return;
       }

       let yearDifference = endDate.getFullYear() - startDate.getFullYear();
       let monthDifference = endDate.getMonth() - startDate.getMonth();

       let difference = (yearDifference*12 + monthDifference);
       let monthYearArray=[];
       let creditSummaryMonth=[];
       let creditSummaryYear=[];

       for(let i = 0 ; i<=difference;i++){
           if(i) startDate.setMonth(startDate.getMonth() + 1);
           monthYearArray[i]= moment(startDate).format("MMMM YYYY");
           creditSummaryMonth[i]= moment(startDate).format("MMMM");
           creditSummaryYear[i]= moment(startDate).format("YYYY");
       }

       let table = `<table class="table table-striped">`;
       for(let i = 0; i < monthYearArray.length; i++){
           table += `<tr>
                           <td>${monthYearArray[i]}</td>
                           <td>
                               <input name="amount[]" type="number" class="form-control" />
                               <input type="hidden" value="${creditSummaryYear[i]}" name="year[]" type="number" class="form-control" />
                               <input type="hidden" value="${creditSummaryMonth[i]}" name="month[]" type="number" class="form-control" />
                           </td>
                       </tr>`;
       }

       table += `</table>`;
       document.getElementById('bankTransactionAmount').innerHTML = table;
       $('.transaction-summery').show();
     });

   $('#bank-start-date,#bank-end-date').trigger('change');


    /************************
     REDIRECT URL START HERE
     ************************/
    let url = document.location.toString();
    if (url.match('#')) {
        $('.nav-pills a[href="#' + url.split('#')[1] + '"]').tab('show');
    }

    /************************
     NON BD HIDE EFFECT HERE
     ************************/
    $('#nonBDDiv').hide();
    $("#nonBD").trigger("change");

    /***************************
     ASSET DETAILS HIDE EFFECT
     *************************/
    $('.land-building').hide();
    $('.apartment').hide();
    $('.vacant').hide();
    $(".asset-type").trigger("change");
});

/***************
 NON BD TRIGGER
 ***************/
$(document).on('change','#nonBD',function(){
    if($(this).prop("checked")) {
        $('#nonBDDiv').show();
    } else {
        $('#nonBDDiv').hide();
    }
});

/******************************
 PROFESSIONAL INFO HIDE EFFECT
 *****************************/
$(document).on("change","#applicantType", function() {
    $('#serviceDetails').hide();
    $('#businessDetails').hide();
    $('#landlordDetails').hide();
    $('#professionalDetails').hide();
    $('#others').hide();

    let applicantType = $(this).val();
    if (applicantType == 'Services') { //Services Details
        $('#serviceDetails').show();
        $('#businessDetails').hide();
        $('#landlordDetails').hide();
        $('#professionalDetails').hide();
        $('#others').hide();

    } else if (applicantType == 'Business') { //Business Details
        $('#serviceDetails').hide();
        $('#businessDetails').show();
        $('#landlordDetails').hide();
        $('#professionalDetails').hide();
        $('#others').hide();

    } else if (applicantType == 'Landlord') { //Landlord Details
        $('#serviceDetails').hide();
        $('#businessDetails').hide();
        $('#landlordDetails').show();
        $('#professionalDetails').hide();
        $('#others').hide();

    }else if(applicantType == 'Professional'){ //Professional Details
        $('#serviceDetails').hide();
        $('#businessDetails').hide();
        $('#landlordDetails').hide();
        $('#professionalDetails').show();
        $('#others').hide();
    }else{ // Others
        $('#serviceDetails').hide();
        $('#businessDetails').hide();
        $('#landlordDetails').hide();
        $('#professionalDetails').hide();
        $('#others').show();
    }
});
$("#applicantType").trigger("change");


/******************************
 CONTACT SAME PERMANENT ADDRESS INFO HIDE EFFECT
 *****************************/
$(document).on("change","#contact-type", function() {
    let type = $(this).val();
    if (type == 'Present' || type == '') {
        $('#is_same_permanent').show();
      }else {
        $('#is_same_permanent').hide();
      }
});
$("#contact-type").trigger("change");

/***************************
 ASSET DETAILS INFO TRIGGER
 **************************/
$(document).on("change",".asset-type", function() {
    let assetType = $(this).val();
    if(assetType == 'Land & Building'){ // Land & Building
        $('.land-building').show();
        $('.apartment').hide();
        $('.vacant').hide();
    }else if(assetType == 'Apartment'){ // Apartment
        $('.land-building').hide();
        $('.apartment').show();
        $('.vacant').hide();
    }else if(assetType == 'Vacant Land'){ // Vacant Land
        $('.land-building').hide();
        $('.apartment').hide();
        $('.vacant').show();
    }else{ // Others
        $('.land-building').hide();
        $('.apartment').hide();
        $('.vacant').hide();
        $('#asset_details_description').val('');
    }
});

/******************************
 BRANCH CODE ONCHANGE SELECT
 ****************************/
 $(".branchId").change(function () {
     let route = "/settings/code-by-branch";
     let parenttHtml = $(this).parent().parent().parent().parent();
     codeByBranch(this,route,parenttHtml);
 });

/*****************************************
 BANK WISE ACCOUNT TYPE ONCHANGE SELECT
 ****************************************/
$(".bankId").change(function () {
    let route = "/settings/account-types-by-bank";
    let targetHtml = '.bankTypeId';
    bankTypesByBank(this,route,targetHtml);
});

/*******************************************
 CONTACT NUMBER VERIFICATION GREATER THAN 0
 ******************************************/
// $('.firstFiveDigit,.secondFiveDigit').on("keyup", function () {
//     $(this).each(function(){
//         let val = parseInt($(this).val());
//         if(val < 0){
//             return false;
//         }
//     });
// });

/**********************************
Asset-Details->Asset Type -> Apartment
********************************/
$(document).on('keyup','#apartment_no,#apartment_size,#proportionate_land,#apartment_address',function(){
  let apartment_no = $('#apartment_no').val();
  let apartment_size = $('#apartment_size').val();
  let proportionate_land = $('#proportionate_land').val();
  let apartment_address = $('#apartment_address').val();
  let apartment_info = 'Apertment No: '+apartment_no+', Apartment Size(SFT): '+apartment_size+', Proportion Land(Decimal): '+proportionate_land+', Apartment Address: '+apartment_address
  $('#asset_details_description').val(apartment_info);
})

/**********************************
Asset-Details->Asset Type -> Land and Building
********************************/
$(document).on('keyup','#land_area,#construction_area_per_floor,#number_of_floor,#land_address',function(){
  let land_area = $('#land_area').val();
  let construction_area_per_floor = $('#construction_area_per_floor').val();
  let number_of_floor = $('#number_of_floor').val();
  let land_address = $('#land_address').val();

  let land_and_building_info = 'Land Area (Decimal): '+land_area+', Construction Area Per Floor (SFT): '+construction_area_per_floor+', Number of Floor: '+number_of_floor+', Land Address: '+land_address
  $('#asset_details_description').val(land_and_building_info);
})

/***************************************
 SOURCE DETAILS SOURCE ID EFFECR
 **************************************/
 $(document).on("keyup", ".sourceId", function () {
    let parentHtml = $(this).parent().parent();
    let route = "/settings/employee-name-by-employee-id";
    employyeNameByEmpId(this,route,parentHtml);
});


// /*****************************
//  TYPE DESIGNATION MODAL LOAD
//  *****************************/
// $(document.body).on('click','.AppModalGroup',function(e){
//     e.preventDefault();
//      $('#groupModalContent').load(
//         $(this).attr('redirect-url'),
//         function (response, status, xhr) {
//             if (status === 'error') {
//                 $('#groupModalContent').html('<p>Sorry, but there was an error:' + xhr.status + ' ' + xhr.statusText + '</p>');
//             }
//             return this;
//         }
//     );
// });

/**********************************
Asset-Details->Asset Type -> Vacant Land
********************************/
$(document).on('keyup','#vacant_land_area,#type_of_land,#vacant_land_address',function(){
  let vacant_land_area = $('#vacant_land_area').val();
  let type_of_land = $('#type_of_land').val();
  let vacant_land_address = $('#vacant_land_address').val();

  let vacant_land_info = 'Vacant Land Area (Decimal): '+vacant_land_area+', Type of Land: '+type_of_land+', Vacant Land Address: '+vacant_land_address;
  $('#asset_details_description').val(vacant_land_info);
})

/******************************************************
 INCOME DETAILS - INCOME TYPE WISE ALL BUSINESS INCOME
 *****************************************************/
$('.income_type').on('change', function () {
    let incomeType = $(this).val();
    let parentHtml = $(this).parent().parent().parent();
    let customerProfileId = parentHtml.find('.customerProfileId').val();
    if(incomeType == 'Business income'){
        let params = { 'customer_profile_id': customerProfileId };
        let route = "/settings/customer-wise-business-income/";
        getBusinessIncomeByCustomer(this, route, params, parentHtml);
    }else
        $('#monthlyIncomeAmount').val('');
});
$('.income_type').trigger('change');
